#include "skyviewfactors.hpp"
#include "BS_thread_pool.hpp"
#include "cpl_conv.h"
#include "gdal_priv.h"
#include "utils.hpp"
#include <atomic>
#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <deque>
#include <filesystem>
#include <iostream>
#include <memory>
#include <mutex>
#include <vector>

#ifdef _WIN32
#include "getopt.hpp"
#endif

#ifdef __APPLE__
#include "unistd.h"
#endif

#ifdef __linux__
#include "unistd.h"
#endif

int main(int argc, char *argv[]) {
    Dataset DSMVegDataset;
    GDALDataset *poDstDS = nullptr;
    GDALDriver *poDriver = nullptr;
    GDALAllRegister();
    auto myconf = getOptions(argc, argv);
    Dataset DSMBuildDataset(myconf.in_dsm);
    std::filesystem::path outpath(myconf.out);
    auto svfpath = outpath / "svf";
    if (!std::filesystem::exists(svfpath)){
        if (!std::filesystem::create_directories(svfpath)) {
            std::cout << "Cannot create svfoutput directory: " << svfpath << std::endl;
            std::exit(-1);
        }
    }
    myconf.out = svfpath;
    if (myconf.isVeg) {
        DSMVegDataset.init(myconf.in_veg);
        svfdsmveg(myconf, DSMBuildDataset, DSMVegDataset);
    } else {
        svfdsm(myconf, DSMBuildDataset);
    }
    return EXIT_SUCCESS;
}

void svfdsmveg(const svfconfig &myconf, Dataset &DSMBuildDataset, Dataset &VegDataset) {
    auto dsm = DSMBuildDataset.getEigenMatrix();
    auto veg = VegDataset.getEigenMatrix();
    int index = 0;
    svfvegbuild svf(dsm.rows(), dsm.cols());
    auto vegmax = veg.maxCoeff();
    auto amaxvalue = dsm.maxCoeff();
    amaxvalue = std::max(vegmax, amaxvalue);
    Eigen::MatrixXf vegdem = dsm + veg;
    vegdem = (vegdem.array() == dsm.array()).select(0, vegdem);
    Eigen::MatrixXf vegdem2 = dsm + (veg * myconf.trunkratio);
    vegdem2 = (vegdem2.array() == dsm.array()).select(0, vegdem2);
    Eigen::MatrixXf bush = (vegdem2.array() * vegdem.array()).unaryExpr([](float elem) {
        return static_cast<float>(!elem);
    }) * vegdem.array();

    auto mypatch = getPatches(myconf.patchoption);
    std::vector<float> temp;
    iAzi(mypatch, temp);
    Eigen::Map<Eigen::VectorXf> iazimuth(temp.data(), temp.size());
    Eigen::VectorXf aziintervalaniso(mypatch.patches_in_band.size());
    for (unsigned int i = 0; i != aziintervalaniso.size(); i++) {
        aziintervalaniso(i) = std::ceil(mypatch.patches_in_band(i) / 2.0);
    }
    std::vector<Eigen::MatrixXf> shmat;
    std::deque<std::future<resultsVeg>> myresults;
    BS::thread_pool mythreads;
    submitJobsDSMVeg(&DSMBuildDataset.geoTransform[0], dsm, vegdem, vegdem2, bush, mypatch, index, iazimuth, myresults,
                     mythreads);
    calculateSvfVegDSM(mypatch, svf, aziintervalaniso, myresults);
    Eigen::MatrixXf last = Eigen::MatrixXf::Constant(dsm.rows(), dsm.cols(), 0.0);
    last = (vegdem2.array() == 0).select(3.0459e-4, last);
    svf.svfSveg = (svf.svfSveg + last).eval();
    svf.svfSaveg = (svf.svfSaveg + last).eval();
    svf.svfWveg = (svf.svfWveg + last).eval();
    svf.svfWaveg = (svf.svfWaveg + last).eval();
    svf.svfS = (svf.svfS.array() + 3.0459e-4);
    svf.svfW = (svf.svfW.array() + 3.0459e-4);
    normToOne(svf);
    saveNewFile(DSMBuildDataset, svf.svf, (myconf.out + "/svf.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfN, (myconf.out + "/svfN.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfW, (myconf.out + "/svfW.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfS, (myconf.out + "/svfS.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfE, (myconf.out + "/svfE.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfveg, (myconf.out + "/svfveg.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfaveg, (myconf.out + "/svfaveg.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfEveg, (myconf.out + "/svfEveg.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfSveg, (myconf.out + "/svfSveg.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfWveg, (myconf.out + "/svfWveg.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfNveg, (myconf.out + "/svfNveg.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfEaveg, (myconf.out + "/svfEaveg.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfSaveg, (myconf.out + "/svfSaveg.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfWaveg, (myconf.out + "/svfWaveg.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfNaveg, (myconf.out + "/svfNaveg.tif").c_str());
    Eigen::MatrixXf svftotal = (svf.svf.array() - (1 - svf.svfveg.array()) * (1 - myconf.transmissivity));
    saveNewFile(DSMBuildDataset, svftotal, (myconf.out + "/Skyviewfactors.tif").c_str());
}

void submitJobsDSMVeg(double *geoTransform, Eigen::Map<Eigen::MatrixXf> &dsm, Eigen::MatrixXf &veg,
                      Eigen::MatrixXf &veg2, Eigen::MatrixXf &bush, patches &mypatch, int &index,
                      Eigen::Map<Eigen::VectorXf> &iazimuth, std::deque<std::future<resultsVeg>> &myresults,
                      BS::thread_pool &mythreads) {

    for (unsigned int i = 0; i != mypatch.skyvaultaltint.size(); i++) {
        for (unsigned int j = 0; j != mypatch.patches_in_band(i); j++) {
            auto altitude = mypatch.skyvaultaltint(i);
            auto azimuth = iazimuth(index);
            myresults.push_back(
                mythreads.submit(shadowingVeg, dsm, veg, veg2, bush, azimuth, altitude, 1 / geoTransform[1], index, i));
            index++;
        }
    }
    // std::cout << iazimuth(15) << " " << mypatch.skyvaultaltint(2) << std::endl;
    // myresults.push_back(mythreads.submit(shadowingVeg, dsm, veg, veg2, bush, iazimuth(15), mypatch.skyvaultaltint(2),
    // 1 / geoTransform[1], index, 1));
}

void svfdsm(const svfconfig &myconf, Dataset &DSMBuildDataset) {
    auto dsm = DSMBuildDataset.getEigenMatrix();
    auto mypatch = getPatches(myconf.patchoption);
    int index = 0;
    std::vector<float> temp;
    iAzi(mypatch, temp);
    Eigen::Map<Eigen::VectorXf> iazimuth(temp.data(), temp.size());
    svfnonveg svf(dsm.rows(), dsm.cols());
    Eigen::VectorXf aziintervalaniso(mypatch.patches_in_band.size());
    for (unsigned int i = 0; i != aziintervalaniso.size(); i++) {
        aziintervalaniso(i) = std::ceil(mypatch.patches_in_band(i) / 2.0);
    }
    std::vector<Eigen::MatrixXf> shmat;
    std::deque<std::future<resultNonVeg>> myresults;
    BS::thread_pool mythreads;
    submitJobsDSM(&DSMBuildDataset.geoTransform[0], dsm, mypatch, index, iazimuth, myresults, mythreads);
    //    mythreads.wait_for_tasks();
    calculateSvfDSM(mypatch, svf, aziintervalaniso, myresults);
    svf.svfS = (svf.svfS.array() + 3.0459e-4);
    svf.svfW = (svf.svfW.array() + 3.0459e-4);
    normToOne(svf);
    saveNewFile(DSMBuildDataset, svf.svf, (myconf.out + "/svf.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfN, (myconf.out + "/svfN.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfW, (myconf.out + "/svfW.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfS, (myconf.out + "/svfS.tif").c_str());
    saveNewFile(DSMBuildDataset, svf.svfE, (myconf.out + "/svfE.tif").c_str());
}

void submitJobsDSM(double *geoTransform, Eigen::Map<Eigen::MatrixXf> &dsm, patches &mypatch, int &index,
                   Eigen::Map<Eigen::VectorXf> &iazimuth, std::deque<std::future<resultNonVeg>> &myresults,
                   BS::thread_pool &mythreads) {
    for (unsigned int i = 0; i != mypatch.skyvaultaltint.size(); i++) {
        for (unsigned int j = 0; j != mypatch.patches_in_band(i); j++) {
            auto altitude = mypatch.skyvaultaltint(i);
            auto azimuth = iazimuth(index);
            myresults.push_back(
                mythreads.submit(shadowingGlobalRadiation, dsm, azimuth, altitude, 1 / geoTransform[1], index, i));
            index++;
        }
    }
}

svfconfig getOptions(int &argc, char **argv) {
    auto showhelp = []() {
        std::cout << "Usage: " << std::endl;
        std::cout << "./skyviewfactors -d [dsm building] -v [vegetation dsm] -t [trunkratio vegdsm, optional] -t "
                     "[transmissivity vegdsm, optional] -o [outputdirectory] -p [Patchoption 1-4] "
                  << std::endl;
        std::cout << "---------------------------------------------------" << std::endl;
        std::cout << "-d: dsm file with buildings" << std::endl;
        std::cout << "-v: vegetation file (optional)" << std::endl;
        std::cout << "-o: output directory(part)" << std::endl;
        std::cout << "-t: trunkratio in % (only used with vegdsm), default 25%" << std::endl;
        std::cout << "-T: transmissivity in % of light through vegetation, default 3% " << std::endl;
        std::cout << "-p: Patchoption 1-4 -> See source ;-)" << std::endl;
    };
    svfconfig myconf;
    myconf.patchoption = 2;
    myconf.trunkratio = 0.25;
    myconf.transmissivity = 0.03;
    myconf.isVeg = false;
    int opt;
    while ((opt = getopt(argc, argv, "d:v:o:p:t:")) != -1) {
        switch (opt) {
        case 'd':
            myconf.in_dsm = optarg;
            break;
        case 'v':
            myconf.in_veg = optarg;
            myconf.isVeg = true;
            break;
        case 'o':
            myconf.out = optarg;
            break;
        case 'p':
            myconf.patchoption = std::stoi(optarg);
            break;
        case 't':
            myconf.trunkratio = std::stof(optarg) / 100.0;
            break;
        case 'T':
            myconf.transmissivity = std::stof(optarg) / 100.0;
            break;
        default:
            showhelp();
            exit(EXIT_FAILURE);
        }
    }
    if (myconf.in_dsm == "" || myconf.out == "" || myconf.patchoption > 4 || myconf.patchoption < 1) {
        showhelp();
        exit(EXIT_FAILURE);
    }
    return myconf;
}
