#include "utils.hpp"
#include "cpl_string.h"
#include "gdal_priv.h"
#include <cstddef>
#include <cstdlib>
#include <iostream>

patches getPatches(unsigned int option) {
    patches mypatches;
    switch (option) {
    case 1: {
        mypatches.annulino.resize(9);
        mypatches.skyvaultaltint.resize(8);
        mypatches.azistart.resize(8);
        mypatches.patches_in_band.resize(8);
        mypatches.annulino << 0.0, 12.0, 24.0, 36.0, 48.0, 60.0, 72.0, 84.0, 90.0;
        mypatches.skyvaultaltint << 6, 18, 30, 42, 54, 66, 78, 90;
        mypatches.azistart << 0, 4, 2, 5, 8, 0, 10, 0;
        mypatches.patches_in_band << 30, 30, 24, 24, 18, 12, 6, 1;
        break;
    }
    case 2: {
        mypatches.annulino.resize(9);
        mypatches.skyvaultaltint.resize(8);
        mypatches.azistart.resize(8);
        mypatches.patches_in_band.resize(8);
        mypatches.annulino << 0, 12, 24, 36, 48, 60, 72, 84, 90;
        mypatches.skyvaultaltint << 6, 18, 30, 42, 54, 66, 78, 90;
        mypatches.azistart << 0, 4, 2, 5, 8, 0, 10, 0;
        mypatches.patches_in_band << 31, 30, 28, 24, 19, 13, 7, 1;
        break;
    }
    case 3: {
        mypatches.annulino.resize(9);
        mypatches.skyvaultaltint.resize(8);
        mypatches.azistart.resize(8);
        mypatches.patches_in_band.resize(8);
        mypatches.annulino << 0, 12, 24, 36, 48, 60, 72, 84, 90;
        mypatches.skyvaultaltint << 6, 18, 30, 42, 54, 66, 78, 90;
        mypatches.azistart << 0, 4, 2, 5, 8, 0, 10, 0;
        mypatches.patches_in_band << 31 * 2, 30 * 2, 28 * 2, 24 * 2, 19 * 2, 13 * 2, 7 * 2, 1;
        break;
    }
    case 4: {
        mypatches.annulino.resize(16);
        mypatches.skyvaultaltint.resize(15);
        mypatches.azistart.resize(15);
        mypatches.patches_in_band.resize(15);
        mypatches.annulino << 0, 4.5, 9, 15, 21, 27, 33, 39, 45, 51, 57, 63, 69, 75, 81, 90;
        mypatches.skyvaultaltint << 3, 9, 15, 21, 27, 33, 39, 45, 51, 57, 63, 69, 75, 81, 90;
        mypatches.azistart << 0, 0, 4, 4, 2, 2, 5, 5, 8, 8, 0, 0, 10, 10, 0;
        mypatches.patches_in_band << 31 * 2, 31 * 2, 30 * 2, 30 * 2, 28 * 2, 28 * 2, 24 * 2, 24 * 2, 19 * 2, 19 * 2,
            13 * 2, 13 * 2, 7 * 2, 7 * 2, 1;
        break;
    }
    default: {
        std::cout << "Error" << std::endl;
        break;
    }
    }
    mypatches.skyvaultaziint = 360 / mypatches.patches_in_band.cast<float>().array();
    std::vector<float> azi;
    std::vector<int> alt;
    for (size_t j = 0; j != mypatches.skyvaultaltint.size(); j++) {
        for (auto k = 0; k != mypatches.patches_in_band(j); k++) {
            alt.push_back(mypatches.skyvaultaltint(j));
            azi.push_back((k * mypatches.skyvaultaziint(j) + mypatches.azistart(j)));
        }
    }
    mypatches.skyvaultalt = Eigen::Map<Eigen::VectorXi>(alt.data(), alt.size());
    mypatches.skyvaultazi = Eigen::Map<Eigen::VectorXf>(azi.data(), alt.size());
    return mypatches;
}

void iAzi(patches &mypatch, std::vector<float> &temp) {
    for (unsigned int j = 0; j != mypatch.skyvaultaziint.size(); j++) {
        for (unsigned int k = 0; k != mypatch.patches_in_band(j); k++) {
            auto tempval = k * mypatch.skyvaultaziint(j) + mypatch.azistart(j);
            if (tempval > 360) {
                tempval = tempval - 360;
            }
            temp.push_back(tempval);
        }
    }
}

void saveNewFile(Dataset &ds, Eigen::MatrixXf &svf, const char *destfile) {
    GDALDriver *poDriver;
    GDALDataset *poDstDS;
    auto pszFormat = "GTiff";
    poDriver = GetGDALDriverManager()->GetDriverByName(pszFormat);
    char **papszOptions = nullptr;
    papszOptions = CSLSetNameValue(papszOptions, "COMPRESS", "DEFLATE");
    papszOptions = CSLSetNameValue(papszOptions, "PREDICTOR", "3");
    poDstDS = poDriver->Create(destfile, ds.XSize, ds.YSize, 1, GDT_Float32, papszOptions);
    if (poDstDS == nullptr) {
        std::cout << "Error, cannot create out dataset: " << destfile << std::endl;
        std::cout << "Exiting." << std::endl;
        std::exit(EXIT_FAILURE);
    }
    poDstDS->SetGeoTransform(ds.geoTransform);
    poDstDS->SetProjection(ds.projref.c_str());
    auto destBand = poDstDS->GetRasterBand(1);
    destBand->RasterIO(GF_Write, 0, 0, ds.XSize, ds.YSize, svf.data(), ds.XSize, ds.YSize, GDT_Float32, 0, 0);
    GDALClose((GDALDatasetH)poDstDS);
}

void outputPatches(patches &mypatch) {
    std::cout << "+++++++++++++++++++++++" << std::endl;
    std::cout << "skyvaultalt: " << mypatch.skyvaultalt.transpose() << std::endl;
    std::cout << "skyvaultazi: " << mypatch.skyvaultazi.transpose() << std::endl;
    std::cout << "annulino: " << mypatch.annulino.transpose() << std::endl;
    std::cout << "skyvaultaltint: " << mypatch.skyvaultaltint.transpose() << std::endl;
    std::cout << "patches_in_band: " << mypatch.patches_in_band.transpose() << std::endl;
    std::cout << "skyvaultaziint: " << mypatch.skyvaultaziint.transpose() << std::endl;
    std::cout << "azistart: " << mypatch.azistart.transpose() << std::endl;
    std::cout << "+++++++++++++++++++++++" << std::endl;
}

void normToOne(svfnonveg &svf) {
    svf.svf = svf.svf.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfN = svf.svfN.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfW = svf.svfW.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfS = svf.svfS.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfE = svf.svfE.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
}

void normToOne(svfvegbuild &svf){
    svf.svf = svf.svf.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfN = svf.svfN.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfW = svf.svfW.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfS = svf.svfS.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfE = svf.svfE.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfveg = svf.svfveg.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfaveg = svf.svfaveg.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfEveg = svf.svfEveg.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfSveg = svf.svfSveg.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfWveg = svf.svfWveg.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfNveg = svf.svfNveg.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfEaveg =svf.svfEaveg.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfSaveg = svf.svfSaveg.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfWaveg = svf.svfWaveg.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
    svf.svfNaveg = svf.svfNaveg.unaryExpr([](float elem) { return elem > 1 ? 1 : elem; });
}

double annulus_weight(float altitude, float aziinterval) {
    auto n = 90;
    auto steprad = (360. / aziinterval) * deg2rad;
    auto annulus = 91. - altitude;
    auto w = (1. / (2. * M_PI)) * std::sin(M_PI / (2. * n)) * std::sin((M_PI * (2. * annulus - 1.)) / (2. * n));
    return steprad * w;
}

angleresult svf_angles_100121() {
    auto vazi1 = arange(1.f, 360.f, 360.f / 16.f);
    auto vazi2 = arange(12.f, 360.f, 360.f / 16.f);
    auto vazi3 = arange(5.f, 360.f, 360.f / 32.f);
    auto vazi4 = arange(2.f, 360.f, 360.f / 32.f);
    auto vazi5 = arange(4.f, 360.f, 360.f / 40.f);
    auto vazi6 = arange(7.f, 360.f, 360.f / 48.f);
    auto vazi7 = arange(6.f, 360.f, 360.f / 48.f);
    auto vazi8 = arange(1.f, 360.f, 360.f / 48.f);
    auto vazi9 = arange(4.f, 359.f, 360.f / 52.f);
    auto vazi10 = arange(5.f, 360.f, 360.f / 52.f);
    auto vazi11 = arange(1.f, 360.f, 360.f / 48.f);
    auto vazi12 = arange(0.f, 359.f, 360.f / 44.f);
    auto vazi13 = arange(3.f, 360.f, 360.f / 44.f);
    auto vazi14 = arange(2.f, 360.f, 360.f / 40.f);
    auto vazi15 = arange(7.f, 360.f, 360.f / 32.f);
    auto vazi16 = arange(3.f, 360.f, 360.f / 24.f);
    auto vazi17 = arange(10.f, 360.f, 360.f / 16.f);
    auto vazi18 = arange(19.f, 360.f, 360.f / 12.f);
    auto vazi19 = arange(17.f, 360.f, 360.f / 8.f);

    aziVecMap azi1(vazi1.data(), vazi1.size());
    aziVecMap azi2(vazi2.data(), vazi2.size());
    aziVecMap azi3(vazi3.data(), vazi3.size());
    aziVecMap azi4(vazi4.data(), vazi4.size());
    aziVecMap azi5(vazi5.data(), vazi5.size());
    aziVecMap azi6(vazi6.data(), vazi6.size());
    aziVecMap azi7(vazi7.data(), vazi7.size());
    aziVecMap azi8(vazi8.data(), vazi8.size());
    aziVecMap azi9(vazi9.data(), vazi9.size());
    aziVecMap azi10(vazi10.data(), vazi10.size());
    aziVecMap azi11(vazi11.data(), vazi11.size());
    aziVecMap azi12(vazi12.data(), vazi12.size());
    aziVecMap azi13(vazi13.data(), vazi13.size());
    aziVecMap azi14(vazi14.data(), vazi14.size());
    aziVecMap azi15(vazi15.data(), vazi15.size());
    aziVecMap azi16(vazi16.data(), vazi16.size());
    aziVecMap azi17(vazi17.data(), vazi17.size());
    aziVecMap azi18(vazi18.data(), vazi18.size());
    aziVecMap azi19(vazi19.data(), vazi19.size());
    Eigen::Vector<float, 1> azi20{0};
    aziVec iazimuth(azi1.size() + azi2.size() + azi3.size() + azi4.size() + azi5.size() + azi6.size() + azi7.size() +
                    azi8.size() + azi9.size() + azi10.size() + azi11.size() + azi12.size() + azi13.size() +
                    azi14.size() + azi15.size() + azi16.size() + azi17.size() + azi18.size() + azi19.size() +
                    azi20.size());
    iazimuth << azi1, azi2, azi3, azi4, azi5, azi6, azi7, azi8, azi9, azi10, azi11, azi12, azi13, azi14, azi15, azi16,
        azi17, azi18, azi19, azi20;
    Eigen::Vector<float, 20> aziinterval{16.f, 16.f, 32.f, 32.f, 40.f, 48.f, 48.f, 48.f, 52.f, 52.f,
                                         48.f, 44.f, 44.f, 40.f, 32.f, 24.f, 16.f, 12.f, 8.f,  1.f};
    return std::make_tuple(iazimuth, aziinterval);
}
