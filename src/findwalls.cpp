#include "dataset.hpp"
#include <tuple>
#define cimg_display 0
#include "BS_thread_pool.hpp"
#include "CImg.h"
#include "cpl_conv.h"
#include "findwalls.hpp"
#include "gdal_priv.h"
#include "templates.hpp"
#include <Eigen/src/Core/ArithmeticSequence.h>
#include <Eigen/src/Core/Matrix.h>
#include <Eigen/src/Core/util/IndexedViewHelper.h>
#include <algorithm>
#include <array>
#include <atomic>
#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <deque>
#include <filesystem>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#ifdef _WIN32
#include "getopt.hpp"
#endif

#ifdef __APPLE__
#include "unistd.h"
#endif

#ifdef __linux__
#include "unistd.h"
#endif

using namespace cimg_library;

int main(int argc, char *argv[]) {
    auto myconf = getOptions(argc, argv);
    GDALAllRegister();
    Dataset poDataset(myconf.in);
    std::cout << poDataset;
    auto scale = 1 / poDataset.geoTransform[1];
    std::cout << std::endl;
    auto dsm = poDataset.getEigenMatrix();
    auto walls = getWalls(dsm, myconf.walllimit);
    std::filesystem::path outpath(myconf.out);
    auto wallpath = outpath / "walls";
    if (!std::filesystem::exists(wallpath)) {

        if (!std::filesystem::create_directories(wallpath)) {
            std::cout << "Cannot create walloutpath directory: " << wallpath << std::endl;
            std::exit(-1);
        }
    }
    myconf.out = wallpath;
    if (myconf.calculateAspect) {
        auto aspect = getAspect(dsm, walls, scale);
        auto [fx, fy] = getGradient(dsm, scale, scale);
        saveNewFile(poDataset, fx, (myconf.out + "/gradientx.tif").c_str());
        saveNewFile(poDataset, fy, (myconf.out + "/gradienty.tif").c_str());
        saveNewFile(poDataset, aspect, (myconf.out + "/aspect.tif").c_str());
    }
    //    std::cout << fx << std::endl;
    saveNewFile(poDataset, walls, (myconf.out + "/walls.tif").c_str());
}

Eigen::MatrixXf getWalls(Eigen::Map<Eigen::MatrixXf> &dsm, double walllimit) {
    Eigen::MatrixXf walls = Eigen::MatrixXf::Constant(dsm.rows(), dsm.cols(), 0);
    for (const auto &i : arange(1, static_cast<int>(dsm.rows()) - 2)) {
        for (const auto &j : arange(1, static_cast<int>(dsm.cols()) - 2)) {
            auto dom = dsm(Eigen::seq(i - 1, i + 1), Eigen::seq(j - 1, j + 1));
            std::array<float, 4> search = {dom(0, 1), dom(1, 0), dom(1, 2), dom(2, 1)};
            walls(i, j) = *std::max_element(search.begin(), search.end());
        }
    }
    walls = (walls - dsm).eval();
    walls = walls.unaryExpr([&walllimit](float elem) { return elem < walllimit ? 0 : elem; });
    for (auto row : walls.rowwise()) {
        row(0) = 0;
        row(row.cols() - 1) = 0;
    }
    for (auto col : walls.colwise()) {
        col(0) = 0;
        col(col.rows() - 1) = 0;
    }
    return walls;
}

Eigen::MatrixXf getAspect(Eigen::Map<Eigen::MatrixXf> &dsm, Eigen::MatrixXf &w, double scale) {
    auto filtersize = static_cast<int>(std::floor((scale + 0.0000000001) * 9));
    if (filtersize <= 2)
        filtersize = 3;
    else {
        if (filtersize != 9) {
            if (filtersize % 2 == 0)
                filtersize++;
        }
    }
    auto walls = Eigen::MatrixXf(w);
    auto filthalveceil = static_cast<int>(std::ceil(filtersize / 2.));
    auto filthalvefloor = static_cast<int>(std::floor(filtersize / 2.));
    Eigen::MatrixXf filtmatrix = Eigen::MatrixXf::Constant(filtersize, filtersize, 0);
    Eigen::MatrixXf buildfilt = Eigen::MatrixXf::Constant(filtersize, filtersize, 0);
    filtmatrix(Eigen::all, filthalveceil - 1).array() = 1.0f;
    auto shape = filtmatrix.rows() - 1;
    buildfilt(filthalveceil - 1, Eigen::seq(0, filthalvefloor - 1)).array() = 1.0f;
    buildfilt(filthalveceil - 1, Eigen::seq(filthalveceil, filtersize - 1)).array() = 2.0f;
    Eigen::MatrixXf x = Eigen::MatrixXf::Constant(dsm.rows(), dsm.cols(), 0);
    Eigen::MatrixXf y = Eigen::MatrixXf::Constant(dsm.rows(), dsm.cols(), 0);
    Eigen::MatrixXf z = Eigen::MatrixXf::Constant(dsm.rows(), dsm.cols(), 0);
    std::cout << filtmatrix << std::endl << std::endl;
    std::cout << buildfilt << std::endl << std::endl;
    walls = walls.unaryExpr([](float elem) {
        if (elem > 0)
            return 1.0f;
        else
            return 0.0f;
    });
    for (auto h : arange(0, 180)) {
        auto filtmatrix1 = getRotatedMatrix(filtmatrix, -h);
        auto filtmatrixbuild = getRotatedMatrix(buildfilt, -h);
        auto index = 270 - h;
        switch (h) {
        case 150:
            filtmatrixbuild(Eigen::all, shape).array() = 0.0f;
            break;
        case 30:
            filtmatrixbuild(Eigen::all, shape).array() = 0.0f;
            break;
        case 45:
            filtmatrix1(0, 0) = 1.0f;
            filtmatrix1(shape, shape) = 1.0f;
            break;
        case 135:
            filtmatrix1(0, shape) = 1.0f;
            filtmatrix1(shape, 0) = 1.0f;
            break;
        default:
            break;
        }
        for (auto i : arange(filthalveceil - 1, static_cast<int>(dsm.rows()) - filthalveceil - 1)) {
            for (auto j : arange(filthalveceil - 1, static_cast<int>(dsm.cols()) - filthalveceil - 1)) {
                if (walls(i, j) > 0.5) {
                    auto wallscut = walls(Eigen::seq(i - filthalvefloor, i + filthalvefloor),
                                          Eigen::seq(j - filthalvefloor, j + filthalvefloor))
                                        .array() *
                                    filtmatrix1.array();
                    auto dsmcut = dsm(Eigen::seq(i - filthalvefloor, i + filthalvefloor),
                                      Eigen::seq(j - filthalvefloor, j + filthalvefloor));
                    auto wsum = wallscut.sum();
                    if (z(i, j) < wsum) {
                        z(i, j) = wsum;
                        double sum1{0}, sum2{0};
                        for (auto k = 0; k != dsmcut.rows(); k++) {
                            for (auto m = 0; m != dsmcut.cols(); m++) {
                                switch (static_cast<int>(filtmatrixbuild(k, m))) {
                                case 1:
                                    sum1 += dsmcut(k, m);
                                    break;
                                case 2:
                                    sum2 += dsmcut(k, m);
                                    break;
                                default:
                                    break;
                                }
                            }
                        }
                        if (sum1 > sum2)
                            x(i, j) = 1;
                        else
                            x(i, j) = 2;
                        y(i, j) = h;
                    }
                }
            }
        }
    }

    for (auto i = 0; i != y.rows(); i++) {
        for (auto j = 0; j != y.cols(); j++) {
            if (x(i, j) == 1.0) {
                y(i, j) -= 180;
            }
            if (y(i, j) < 0) {
                y(i, j) += 360;
            }
        }
    }

    auto [grad, asp] = getDers(dsm, scale);
    y.array() += (walls.unaryExpr([](float elem) { return elem == 1.0f ? 1.0f : 0.0f; }).array() *
                  y.unaryExpr([](float elem) { return elem == 0.0f ? 1.0f : 0.0f; }).array() *
                  (asp.array() / (static_cast<float>(M_PI) / 180.0)));
    return y;
}

std::tuple<Eigen::MatrixXf, Eigen::MatrixXf> getGradient(Eigen::Map<Eigen::MatrixXf> &dsm, double dx, double dy) {
    Eigen::MatrixXf mydsm(dsm);
    Eigen::MatrixXf gr_x = Eigen::MatrixXf::Constant(dsm.rows(), dsm.cols(), 0.0);
    Eigen::MatrixXf gr_y = Eigen::MatrixXf::Constant(dsm.rows(), dsm.cols(), 0.0);
    gr_y(Eigen::seq(1, Eigen::last - 1), Eigen::all).array() =
        (mydsm(Eigen::seq(2, Eigen::last), Eigen::all).array() -
         mydsm(Eigen::seq(0, Eigen::last - 2), Eigen::all).array()) /
        2. * dy;
    gr_y(0, Eigen::all).array() = (gr_y(1, Eigen::all).array() - gr_y(0, Eigen::all).array()) / dy;
    gr_y(Eigen::last, Eigen::all).array() =
        (gr_y(Eigen::last - 1, Eigen::all).array() - gr_y(Eigen::last, Eigen::all).array()) / dy;
    gr_x(Eigen::all, Eigen::seq(1, Eigen::last - 1)).array() =
        (mydsm(Eigen::all, Eigen::seq(2, Eigen::last)).array() -
         mydsm(Eigen::all, Eigen::seq(0, Eigen::last - 2)).array()) /
        2. * dx;
    gr_x(Eigen::all, 0).array() = (gr_x(Eigen::all, 1).array() - gr_x(Eigen::all, 0).array()) / dx;
    gr_x(Eigen::all, Eigen::last).array() =
        (gr_x(Eigen::all, Eigen::last - 1).array() - gr_x(Eigen::all, Eigen::last).array()) / dx;

    return std::make_tuple(gr_x, gr_y);
}

std::tuple<Eigen::MatrixXf, Eigen::MatrixXf> getDers(Eigen::Map<Eigen::MatrixXf> &dsm, double scale) {
    auto dx = 1 / scale;
    auto [fx, fy] = getGradient(dsm, dx, dx);
    auto [asp, grad] = cart2pol(fx, fy);
    grad = grad.array().atan().eval();
    asp = asp * -1;
    asp += asp.unaryExpr([](float elem) { return elem < 0 ? static_cast<float>(M_PI * 2) : 0; }).eval();
    return std::make_tuple(grad, asp);
}

std::tuple<Eigen::MatrixXf, Eigen::MatrixXf> cart2pol(Eigen::MatrixXf &x, Eigen::MatrixXf &y) {
    Eigen::MatrixXf radius = (x.array().pow(2) + y.array().pow(2)).sqrt();
    Eigen::MatrixXf theta = Eigen::MatrixXf::Constant(x.rows(), x.cols(), 0);
    for (auto i = 0; i != x.rows(); i++) {
        for (auto j = 0; j != x.cols(); j++) {
            theta(i, j) = std::atan2(y(i, j), x(i, j));
        }
    }
    return std::make_tuple(theta, radius);
}

wallconfig getOptions(int &argc, char **argv) {
    auto showhelp = []() {
        std::cout << "Usage: " << std::endl;
        std::cout << "./findwalls -i [inputfile] -o [outputfile] -w [lower wall limit in m] [-a]" << std::endl;
        std::cout << "------------------------------------------" << std::endl;
        std::cout << "-i: input file" << std::endl;
        std::cout << "-o: output path" << std::endl;
        std::cout << "-w  lower wall limit [m]" << std::endl;
        std::cout << "-a  calculate aspect ration" << std::endl;
    };
    wallconfig myconf;
    myconf.walllimit = 3;
    myconf.calculateAspect = false;
    int opt;
    while ((opt = getopt(argc, argv, "i:o:w:a")) != -1) {
        switch (opt) {
        case 'i':
            myconf.in = optarg;
            break;
        case 'o':
            myconf.out = optarg;
            break;
        case 'w':
            myconf.walllimit = std::stod(optarg);
            break;
        case 'a':
            myconf.calculateAspect = true;
            break;
        default:
            showhelp();
            exit(EXIT_FAILURE);
        }
    }
    if (myconf.in == "" || myconf.out == "") {
        showhelp();
        exit(EXIT_FAILURE);
    }
    return myconf;
}

Eigen::MatrixXf getRotatedMatrix(Eigen::MatrixXf &matrix, int angle) {
    CImg<float> img(matrix.cols(), matrix.rows(), 1, 1);
    for (auto i = 0; i != matrix.rows(); i++) {
        for (auto j = 0; j != matrix.cols(); j++) {
            img(j, i) = matrix(i, j);
        }
    }
    img.rotate(angle, static_cast<int>((img.width()) / 2), static_cast<int>((img.height()) / 2), 0, 1);
    Eigen::MatrixXf rotated(img.height(), img.width());
    for (auto i = 0; i != img.width(); i++) {
        for (auto j = 0; j != img.height(); j++) {
            rotated(j, i) = img(i, j);
        }
    }
    return rotated;
}
