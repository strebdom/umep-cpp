#include "projection.hpp"
#include "common_utils.hpp"
#include "gdal.h"
#include "ogr_core.h"
#include <memory>
#include <type_traits>

Projection::Geotransform Projection::getLatLonGT(ImageParams &parm) {
    Geotransform gt;
    if (debug) std::cout << parm.XSize << " " << parm.YSize << std::endl;
    auto dx = (parm.ur.x - parm.ul.x) / (parm.XSize);
    gt[0] = parm.ul.x - 0.5 * dx;
    gt[1] = dx;
    gt[2] = 0.0;
    auto dy = (parm.ul.y - parm.ll.y) / (parm.YSize);
    gt[3] = parm.ul.y + 0.5 * dy;
    gt[4] = 0.0;
    gt[5] = -dy;
    // gt[4] = (params.lr.y - gt[3] - gt[5] * params.YSize) / params.XSize;
    return gt;
}

Projection::Geotransform Projection::getProjectedGT(ImageParams &parm) {
    Geotransform gt;
    if (debug) std::cout << parm.XSize << " " << parm.YSize << std::endl;
    gt[0] = parm.ul.x - 0.5 * dx;
    gt[1] = dx;
    gt[2] = 0.0;
    gt[3] = parm.ul.y + 0.5 * dy;
    gt[4] = 0.0;
    gt[5] = -dy;
    // gt[4] = (params.lr.y - gt[3] - gt[5] * params.YSize) / params.XSize;
    return gt;
}

std::string Projection::getLambertProjParams(std::map<std::string, std::string> &param) {
    std::stringstream projstringstream, projstringstream_wgs84;
    projstringstream << "+proj=lcc +units=m +a=6370000.0 +b=6370000.0 +lat_1=" << param["TRUELAT1"]
                     << " +lat_2=" << param["TRUELAT2"] << " +lat_0=" << param["MOAD_CEN_LAT"]
                     << " +lon_0=" << param["STAND_LON"];
    projstringstream_wgs84 << "+proj=lcc +units=m +lat_1=" << param["TRUELAT1"] << " +lat_2=" << param["TRUELAT2"]
                           << " +lat_0=" << param["MOAD_CEN_LAT"] << " +lon_0=" << param["STAND_LON"] << " datum=WGS84";
    return projstringstream_wgs84.str();
}

std::string Projection::getProjParams() {
    auto projnumstr = dimds["XLAT"]->GetMetadataItem("NC_GLOBAL#MAP_PROJ");
    int projnum;
    std::map<std::string, std::string> paramlist;
    std::string projstring;
    try {
        projnum = std::stoi(projnumstr);
    } catch (const std::invalid_argument &e) {
        std::cout << e.what() << std::endl;
        exit(EXIT_FAILURE);
    } catch (const std::out_of_range &e) {
        std::cout << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    dx = std::stod(dimds["XLAT"]->GetMetadataItem("NC_GLOBAL#DX"));
    dy = std::stod(dimds["XLAT"]->GetMetadataItem("NC_GLOBAL#DY"));
    switch (projnum) {
    case PROJTYPE::LAMBERT_CONFORMAL:
        paramlist["TRUELAT1"] = std::string(dimds["XLAT"]->GetMetadataItem("NC_GLOBAL#TRUELAT1"));
        paramlist["TRUELAT2"] = std::string(dimds["XLAT"]->GetMetadataItem("NC_GLOBAL#TRUELAT2"));
        paramlist["STAND_LON"] = std::string(dimds["XLAT"]->GetMetadataItem("NC_GLOBAL#STAND_LON"));
        paramlist["MOAD_CEN_LAT"] = std::string(dimds["XLAT"]->GetMetadataItem("NC_GLOBAL#MOAD_CEN_LAT"));
        projstring = getLambertProjParams(paramlist);
        break;
    case PROJTYPE::LAT_LON:
    case PROJTYPE::MERCATOR:
    case PROJTYPE::POLAR_STEREOGRAPHIC:
    default:
        std::cout << "Other projections than Lambert Conformal not yet implemented!" << std::endl;
        exit(EXIT_FAILURE);
    }
    return projstring;
}

void Projection::initSize() {
    imgparams_orig.XSize = dimds["XLONG"]->GetRasterXSize();
    imgparams_orig.YSize = dimds["XLONG"]->GetRasterYSize();
    imgparams_orig.nVals = imgparams_orig.XSize * imgparams_orig.YSize;
    imgparams_out = ImageParams(imgparams_orig);
}

Projection::Projection(std::map<std::string, GDALDataset *> &dims, const int debug) : dimds(dims), debug(debug) {
    initSize();
    prepareBuffer();
    initTransformer();
    initDataMatrices();
    getOrigImageCorners();
    transformImageCorners(imgparams_orig, imgparams_out);
    orig_gt = getLatLonGT(imgparams_orig);
    if (debug) {
        for (const auto &e : orig_gt) {
            std::cout << e << " " << std::endl;
        }
    }
    transformed_gt = getProjectedGT(imgparams_out);
    std::cout << std::endl;
    if (debug) {
        for (const auto &e : transformed_gt) {
            std::cout << e << " " << std::endl;
        }
    }
}

void Projection::initDataMatrices() {
    auto rasterxlong = dimds["XLONG"]->GetRasterBand(1);
    auto rasterxlat = dimds["XLAT"]->GetRasterBand(1);
    auto rasterxlongu = dimds["XLONG_U"]->GetRasterBand(1);
    auto rasterxlongv = dimds["XLONG_V"]->GetRasterBand(1);
    auto rasterxlatu = dimds["XLAT_U"]->GetRasterBand(1);
    auto rasterxlatv = dimds["XLAT_V"]->GetRasterBand(1);
    auto err_xlong = rasterxlong->RasterIO(GF_Read, 0, 0, imgparams_orig.XSize, imgparams_orig.YSize, XLONGBuffer.get(),
                                           imgparams_orig.XSize, imgparams_orig.YSize, GDT_Float32, 0, 0);
    auto err_xlat = rasterxlat->RasterIO(GF_Read, 0, 0, imgparams_orig.XSize, imgparams_orig.YSize, XLATBuffer.get(),
                                         imgparams_orig.XSize, imgparams_orig.YSize, GDT_Float32, 0, 0);
    auto err_xlatu = rasterxlatu->RasterIO(GF_Read, 0, 0, imgparams_orig.XSize, imgparams_orig.YSize, XLATUBuffer.get(),
                                           imgparams_orig.XSize, imgparams_orig.YSize, GDT_Float32, 0, 0);
    auto err_xlatv = rasterxlatv->RasterIO(GF_Read, 0, 0, imgparams_orig.XSize, imgparams_orig.YSize, XLATVBuffer.get(),
                                           imgparams_orig.XSize, imgparams_orig.YSize, GDT_Float32, 0, 0);
    auto err_xlongv =
        rasterxlongv->RasterIO(GF_Read, 0, 0, imgparams_orig.XSize, imgparams_orig.YSize, XLONGVBuffer.get(),
                               imgparams_orig.XSize, imgparams_orig.YSize, GDT_Float32, 0, 0);
    auto err_xlongu =
        rasterxlongu->RasterIO(GF_Read, 0, 0, imgparams_orig.XSize, imgparams_orig.YSize, XLONGUBuffer.get(),
                               imgparams_orig.XSize, imgparams_orig.YSize, GDT_Float32, 0, 0);
    /*utils::flip_vertical(imgparams_orig.YSize, imgparams_orig.XSize, XLONGBuffer.get());
    utils::flip_vertical(imgparams_orig.YSize, imgparams_orig.XSize, XLATBuffer.get());
    utils::flip_vertical(imgparams_orig.YSize, imgparams_orig.XSize, XLATUBuffer.get());
    utils::flip_vertical(imgparams_orig.YSize, imgparams_orig.XSize, XLATVBuffer.get());
    utils::flip_vertical(imgparams_orig.YSize, imgparams_orig.XSize, XLONGUBuffer.get());
    utils::flip_vertical(imgparams_orig.YSize, imgparams_orig.XSize, XLONGVBuffer.get());*/

    XLONGData =
        std::make_unique<Datamatrix>(Datamatrix(XLONGBuffer.release(), imgparams_orig.XSize, imgparams_orig.YSize));
    XLATData =
        std::make_unique<Datamatrix>(Datamatrix(XLATBuffer.release(), imgparams_orig.XSize, imgparams_orig.YSize));
    XLONGUData =
        std::make_unique<Datamatrix>(Datamatrix(XLONGUBuffer.release(), imgparams_orig.XSize, imgparams_orig.YSize));
    XLONGVData =
        std::make_unique<Datamatrix>(Datamatrix(XLONGVBuffer.release(), imgparams_orig.XSize, imgparams_orig.YSize));
    XLATUData =
        std::make_unique<Datamatrix>(Datamatrix(XLATUBuffer.release(), imgparams_orig.XSize, imgparams_orig.YSize));
    XLATVData =
        std::make_unique<Datamatrix>(Datamatrix(XLATVBuffer.release(), imgparams_orig.XSize, imgparams_orig.YSize));
}

void Projection::prepareBuffer() {
    XLONGBuffer = std::make_unique<float[]>(imgparams_orig.nVals);
    XLATBuffer = std::make_unique<float[]>(imgparams_orig.nVals);
    XLATUBuffer = std::make_unique<float[]>(imgparams_orig.nVals);
    XLATVBuffer = std::make_unique<float[]>(imgparams_orig.nVals);
    XLONGUBuffer = std::make_unique<float[]>(imgparams_orig.nVals);
    XLONGVBuffer = std::make_unique<float[]>(imgparams_orig.nVals);
}

void Projection::getOrigImageCorners() {
    imgparams_orig.ul = Coordinate{.x = (*XLONGData)(0, 0), .y = (*XLATData)(0, 0)};
    imgparams_orig.ur = Coordinate{.x = (*XLONGData)(0, Eigen::last), .y = (*XLATData)(0, Eigen::last)};
    imgparams_orig.ll = Coordinate{.x = (*XLONGData)(Eigen::last, 0), .y = (*XLATData)(Eigen::last, 0)};
    imgparams_orig.lr =
        Coordinate{.x = (*XLONGData)(Eigen::last, Eigen::last), .y = (*XLATVData)(Eigen::last, Eigen::last)};
}
void Projection::initTransformer() {
    osr_latlon = OGRSpatialReference();
    osr_latlon.importFromProj4("+proj=latlong +datum=WGS84");
    osr_out = OGRSpatialReference();
    osr_out.importFromProj4(getProjParams().c_str());
    transformer = OGRCreateCoordinateTransformation(&osr_latlon, &osr_out);
}

void Projection::transformImageCorners(ImageParams &parmin, ImageParams &parmout) {
    std::vector<double> transform_long{parmin.ul.x, parmin.ur.x, parmin.ll.x, parmin.lr.x};
    std::vector<double> transform_lat{parmin.ul.y, parmin.ur.y, parmin.ll.y, parmin.lr.y};
    transformer->Transform(transform_long.size(), transform_long.data(), transform_lat.data());
    parmout.ul = Coordinate{.x = transform_long[0], .y = transform_lat[0]};
    parmout.ur = Coordinate{.x = transform_long[1], .y = transform_lat[1]};
    parmout.ll = Coordinate{.x = transform_long[2], .y = transform_lat[2]};
    parmout.lr = Coordinate{.x = transform_long[3], .y = transform_lat[3]};
    if (debug) {
        std::cout << "parmout.llu.x: " << parmout.ul.x << " parmout.llu.y: " << parmout.ul.y << std::endl;
        std::cout << "parmout.llv.x: " << parmout.ur.x << " parmout.llv.y: " << parmout.ur.y << std::endl;
        std::cout << "parmout.lru.x: " << parmout.ll.x << " parmout.lru.y: " << parmout.ll.y << std::endl;
        std::cout << "parmout.ulv.x: " << parmout.lr.x << " parmout.ulv.y: " << parmout.lr.y << std::endl;
    }
}
