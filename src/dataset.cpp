#include "dataset.hpp"

Dataset::Dataset(std::string &path) : path(path) {
    init(path);
}

Dataset::Dataset() {
}

Dataset::~Dataset() {
    GDALClose((GDALDatasetH)dset);
}

void Dataset::init(std::string &path) {
    dset = static_cast<GDALDataset *>(GDALOpen(path.c_str(), GA_ReadOnly));
    if (dset == nullptr) {
        std::cout << "Can't open file " << path << std::endl;
        std::cout << "Exiting" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    driver = dset->GetDriverName();
    dset->GetGeoTransform(geoTransform);
    XSize = dset->GetRasterXSize();
    YSize = dset->GetRasterYSize();
    RasterCount = dset->GetRasterCount();
    projref = dset->GetProjectionRef();
    band = dset->GetRasterBand(1);
    band->GetBlockSize(&nBlockXSize, &nBlockYSize);
}

Eigen::Map<Eigen::MatrixXf> Dataset::getEigenMatrix() {
    auto nXSize = band->GetXSize();
    auto nYSize = band->GetYSize();
    auto pf = std::make_unique<float[]>(nXSize * nYSize);
    band->RasterIO(GF_Read, 0, 0, nXSize, nYSize, pf.get(), nXSize, nYSize, GDT_Float32, 0, 0);
    return Eigen::Map<Eigen::MatrixXf>(pf.release(), nXSize, nYSize);
}

std::ostream &operator<<(std::ostream &os, const Dataset &ds) {
    os << "################################################" << std::endl;
    os << "XSize: " << ds.XSize << " YSize: " << ds.YSize << std::endl
       << " RasterCount: " << ds.RasterCount << std::endl;
    std::cout << "Projection: " << ds.projref << std::endl;
    std::cout << "pixel resolution: " << ds.geoTransform[1] << std::endl;
    return os;
}
