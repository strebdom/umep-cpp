#include "wrfncdataset.hpp"
#include "common_utils.hpp"
#include "gdal.h"
#include "gdal_priv.h"
#include "projection.hpp"
#include <cstdlib>
#include <memory>
#include <vector>

WRFNCDataset::WRFNCDataset(const std::string &filename, const int debug) : filename(filename), debug(debug) {
    if (!file_exists(filename)) {
        std::cerr << "File " << filename << " not existing! Exit" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    GDALAllRegister();
    mainds = openDataset(filename);
    checkDatasetConformity();
    ncvars = getNcvars();
    checkMandatoryVariables();
    initializeMandatoryVariables();
    proj = std::make_unique<Projection>(Projection(vsds, debug));

}

bool WRFNCDataset::checkVariable(const std::string &var) {
    auto it = std::find(ncvars.begin(), ncvars.end(), var);
    if (it != ncvars.end())
        return true;
    else
        return false;
}

inline bool WRFNCDataset::file_exists(const std::string &name) {
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

GDALDataset *WRFNCDataset::openDataset(const std::string &filename) {
    std::cout << "Opening main dataset" << std::endl;
    auto upr = GDALDataset::Open(filename.c_str());
    if (upr == nullptr) {
        std::cerr << "Error, cannot open dataset " << filename << " exiting." << std::endl;
        std::exit(EXIT_FAILURE);
    }
    return upr;
}

std::vector<std::string> WRFNCDataset::getNcvars() {
    std::cout << "Reading variables" << std::endl;
    std::vector<std::string> ncvars;
    auto subdatasets = CPLStringList(mainds->GetMetadata("SUBDATASETS"));
    for (int i = 0; i < subdatasets.Count(); i++) {
        auto strelement = std::string(subdatasets[i]);
        if (strelement.find("NAME") != std::string::npos) {
            auto varIterator = strelement.find("//");
            ncvars.emplace_back(strelement.substr(varIterator + 2, std::string::npos));
        }
    }
    return ncvars;
}

std::vector<std::string> WRFNCDataset::getVars() {
    return ncvars;
}


const std::vector<std::string> & WRFNCDataset::getVarsref(){
    return ncvars;
}

void WRFNCDataset::checkDatasetConformity() {
    std::cout << "Checking conformity of file" << std::endl;
    const char *title = nullptr;
    const char *west_east = nullptr;
    const char *south_north = nullptr;
    if (std::string(mainds->GetDriverName()) != "HDF5") {
        exitWRFDataset("Error, Drivername not HDF5, Exiting");
    }
    title = mainds->GetMetadataItem("TITLE");
    west_east = mainds->GetMetadataItem("WEST-EAST_GRID_DIMENSION");
    south_north = mainds->GetMetadataItem("SOUTH-NORTH_GRID_DIMENSION");
    if (title == nullptr || west_east == nullptr || south_north == nullptr) {
        exitWRFDataset("Error, is this a WRF outputfile? Exiting");
    }
}

inline void WRFNCDataset::exitWRFDataset(const std::string &message) {
    std::cerr << message << std::endl;
    std::exit(EXIT_FAILURE);
}

void WRFNCDataset::checkMandatoryVariables(){
    std::cout << "Checking mandatory variables" << std::endl;
    for (const auto &e: MANDATORY_VARIABLES){
        if (!checkVariable(e)){
            std::string error =  "Error! Variable " + e + " not found! Exiting";
            exitWRFDataset(error);
         }
    }
}

void WRFNCDataset::initializeMandatoryVariables(){
    auto sn = mainds->GetMetadataItem("SOUTH-NORTH_GRID_DIMENSION");
    auto we = mainds->GetMetadataItem("WEST-EAST_GRID_DIMENSION");
    nx = std::stoi(we)-1;
    ny = std::stoi(sn)-1;
    if (debug) std::cout << "nx: " << nx << " ny:" << ny << std::endl; 
    for (auto const &e: MANDATORY_VARIABLES){
        std::stringstream ss;
        ss << R"(NETCDF:")" << filename << R"("://)" << e;
        if (debug) std::cout << ss.str() << std::endl;
        vsds[e] = GDALDataset::Open(ss.str().c_str());
        rsmap[e] = vsds[e]->GetRasterBand(1);
        auto buffer = std::make_unique<float[]>(nx*ny);
        utils::flip_vertical(ny, nx, buffer.get());
        auto ret = rsmap[e]->RasterIO(GF_Read, 0, 0, nx, ny, buffer.get(), nx, ny, GDT_Float32, 0, 0);
        if (ret != 0){
            std::string errormsg{"Error: Variable " + e + " couldn't be read. Exiting"};
            exitWRFDataset(errormsg); 
        }
        dims[e] = std::make_unique<Datamatrix>(Datamatrix(buffer.release(), ny, nx));
    }
}

std::vector<std::array<double, 6>> WRFNCDataset::getGT(){
    std::vector<std::array<double,6>> gt;
    gt.push_back(proj->orig_gt);
    gt.push_back(proj->transformed_gt);
    return gt;
}

std::vector<OGRSpatialReference> WRFNCDataset::getOSR(){
    std::vector<OGRSpatialReference> osr;
    osr.push_back(proj->osr_latlon);
    osr.push_back(proj->osr_out);
    return osr;
}


WRFNCDataset::~WRFNCDataset() {
}