#include "wrf_to_geotiff.hpp"
#include "common_utils.hpp"
#include "gdal_priv.h"
#include "getopt.h"
#include "projection.hpp"
#include "wrfncdataset.hpp"
#include <bits/getopt_core.h>
#include <bits/getopt_ext.h>
#include <cstddef>
#include <cstdlib>
#include <sstream>

using namespace Eigen;

static const struct option long_options[] = {{"input", required_argument, 0, 'i'},
                                             {"variable", required_argument, 0, 'v'},
                                             {"outputfile", required_argument, 0, 'o'},
                                             {"debug", optional_argument, 0, 'd'},
                                             {0, 0, 0, 0}};

void printUsage() {
    std::cout << "Convert 2d variable in wrffile to geotiff" << std::endl;
    std::cout << "##########################" << std::endl;
    std::cout << "Usage: " << std::endl;
    std::cout << "wrf_to_geotiff -i|--input [inputfile] -o|--output [outputfile] -v||--var [2D variable to be "
                 "exported] -d DEBUG"
              << std::endl;
}

std::map<std::string, std::string> parseArguments(int &argc, char **&argv) {
    if (argc == 1) {
        printUsage();
        exit(EXIT_FAILURE);
    }
    int opt = 0;
    int long_index = 0;
    std::map<std::string, std::string> configmap;
    configmap["output"] = "";
    configmap["input"] = "";
    configmap["var"] = "";
    configmap["debug"] = "0";
    while ((opt = getopt_long(argc, argv, "o:i:v:d", long_options, &long_index)) != -1) {
        switch (opt) {
        case 'i':
            configmap["input"] = optarg;
            break;
        case 'o':
            configmap["output"] = optarg;
            break;
        case 'v':
            configmap["var"] = optarg;
            break;
        case 'd':
            configmap["debug"] = "1";
            break;
        default:
            exit(EXIT_FAILURE);
        }
    }
    for (auto &elem : configmap) {
        if (elem.second == "") {
            std::cerr << "You need to set all arguments" << std::endl << std::endl << std::endl;
            printUsage();
            exit(EXIT_FAILURE);
        }
    }
    return configmap;
}

int main(int argc, char *argv[]) {
    int debug = 0;
    GDALAllRegister();
    auto config = parseArguments(argc, argv);
    if (config["debug"] == "1") {
        debug = 1;
    } else {
        CPLPushErrorHandler(CPLQuietErrorHandler);
    }
    std::cout.precision(15);
    WRFNCDataset myds(config["input"], debug);
    auto vars = myds.getVarsref();
    if (debug) {
        for (auto &elem : vars) {
            std::cout << elem << std::endl;
        }
    }
    if (std::find(vars.begin(), vars.end(), config["var"]) == vars.end()) {
        std::cerr << R"(Did not find the variable ")" << config["var"] << R"(" in file ")" << config["input"] << "\""
                  << std::endl;
        std::cerr << "Might also be a 3D variable. For now only 2D variables are supported." << std::endl;
        std::cerr << "Exiting..." << std::endl;
        exit(EXIT_FAILURE);
    }
    std::stringstream ss_file;
    ss_file << "NETCDF:\"" << config["input"] << "\"://" << config["var"];
    GDALDataset *inputvards = nullptr;
    auto Tiffdriver = GetGDALDriverManager()->GetDriverByName("GTiff");

    inputvards = GDALDataset::Open(ss_file.str().c_str());
    if (inputvards == nullptr) {
        std::cerr << "Error: Can't open file" << std::endl;
        std::cerr << "Exiting!" << std::endl;
        exit(EXIT_FAILURE);
    }
    auto T2BAND = inputvards->GetRasterBand(1);
    auto T2Buffer = new float[myds.nx * myds.ny];
    T2BAND->RasterIO(GF_Read, 0, 0, myds.nx, myds.ny, T2Buffer, myds.nx, myds.ny, GDT_Float32, 0, 0);
    char **papszOptions = NULL;
    auto destDs = Tiffdriver->Create(config["output"].c_str(), myds.nx, myds.ny, 1, GDT_Float32, papszOptions);
    if (destDs == nullptr) {
        std::cerr << "Couldn't create outputfile \"" << config["output"] << "\"" << std::endl;
        std::cerr << "Exiting" << std::endl;
        exit(EXIT_FAILURE);
    }
    auto gtds = myds.getGT()[1];
    destDs->SetGeoTransform(gtds.data());
    destDs->SetSpatialRef(&myds.getOSR()[1]);
    auto destBand = destDs->GetRasterBand(1);
    destBand->RasterIO(GF_Write, 0, 0, myds.nx, myds.ny, T2Buffer, myds.nx, myds.ny, GDT_Float32, 0, 0);
    GDALClose(destDs);
}
