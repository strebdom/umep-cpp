//
// Created by Dominik Strebel on 10.11.22.
//

#include "svfcl.hpp"
svfvegbuild::svfvegbuild(int rows, int cols) : rows(rows), cols(cols), svfnonveg(rows, cols) {
    svfveg = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfaveg = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfSveg = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfWveg = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfNveg = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfEveg = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfaveg = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfEaveg = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfSaveg = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfWaveg = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfNaveg = Eigen::MatrixXf::Constant(rows,cols, 0.0);
}
svfnonveg::svfnonveg(int rows, int cols) : rows(rows), cols(cols) {
    svf = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfE =Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfS = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfW = Eigen::MatrixXf::Constant(rows,cols, 0.0);
    svfN = Eigen::MatrixXf::Constant(rows,cols, 0.0);
}
