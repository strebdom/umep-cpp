#include "solweig/interface.hpp"
#include "common_utils.hpp"
#include <cstdlib>

std::string getOptions(int &argc, char **argv) {
    std::string path;
    int opt;
    while ((opt = getopt(argc, argv, "c:")) != -1) {
        switch (opt) {
        case 'c':
            path = optarg;
            break;
        default:
            showHelp();
            exit(EXIT_FAILURE);
        }
    }
    if (path == "") {
        showHelp();
        exit(EXIT_FAILURE);
    }
    return path;
}

void checkConfig(utils::sectionconfigmaptxt &conf) {
    for (const auto &e : requiredParameters) {
        auto sectionit = conf.find(e.first);
        if (sectionit == conf.end()) {
            std::cout << "Error: Required config section '" << e.first << "' was not found" << std::endl;
            std::cout << "Please check config" << std::endl;
            exit(EXIT_FAILURE);
        }
        for (const auto &p : e.second) {
            std::cout << p << std::endl;
            auto pit = conf[e.first].find(p);
             if (pit == conf[e.first].end()) {
                std::cout << "Error: Required config parameter '" << e.first << "': '" << p << "' was not found"
                          << std::endl;
                std::cout << "Please check config" << std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }
}

    void showHelp() {
        std::cout << "Solweig main solver" << std::endl;
        std::cout << "###################" << std::endl;
        std::cout << "Usage: " << std::endl;
        std::cout << "./solweig -c solweig.conf" << std::endl;
        std::cout << "see code for example of solweig.conf" << std::endl;
        std::cout << "###################" << std::endl;
    }