#include "solweig/solweig.hpp"
#include "date/date.h"
#include "date/tz.h"
#include "solweig/interface.hpp"
#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <ostream>
#include <string>
#include <tuple>
using namespace date;
using namespace std::chrono_literals;

int main(int argc, char *argv[]) {
    auto conffile = getOptions(argc, argv);
    auto confmap = utils::parseSectionConfig(conffile);
    checkConfig(confmap);
    posdata pdat;
    location lp;
    tpSol tp;
    setLocPoint(lp, confmap);
    S_init(&pdat);
    auto datetime = make_zoned("Europe/Zurich", date::local_days{month{5} / 15 / 2023} + 0h + 0min);
    auto tt = datetime.get_local_time();
    std::vector<std::tuple<tpSol, double>> altitudes;
    for (int i = 0; i != 3600; i += 15) {
        auto [year, month, day, hour, minute, second, millisecond] = getDateTimeComponents(datetime);
        tp.year = year;
        tp.month = month;
        tp.day = day;
        tp.hour = hour;
        tp.minute = minute;
        tp.second = second;
        getSolarParameters(pdat, tp, lp);
        altitudes.push_back(std::make_tuple(tp, pdat.elevref));
        auto tt = datetime.get_local_time() + 15min;
        datetime = make_zoned("Europe/Zurich", tt);
    }
    auto it = std::max_element(altitudes.begin(), altitudes.end(),
                               [&](std::tuple<tpSol, double> const &s1, std::tuple<tpSol, double> const &s2) {
                                   return std::get<1>(s1) < std::get<1>(s2);
                               });
    std::cout << std::get<1>(*it) << std::endl;

    tzset();
    std::cout << timezone << std::endl;
    std::cout << tzname[0] << std::endl;
    std::cout << tzname[1] << std::endl;
    /* for (auto &e: altitudes){
        std::cout << "az: " << std::get<1>(e) << std::endl;
    }*/
}

void getSolarParameters(posdata &pdat, tpSol &tpa, location &lp) {
    pdat.function = ~S_DOY;
    pdat.longitude = lp.lon;
    pdat.latitude = lp.lat;
    auto tt = make_zoned(lp.tz,floor<std::chrono::milliseconds>(date::clock_cast_detail::system_clock::now()));
    std::sscanf(format("%z", tt).c_str(), "+%2f", &(pdat.timezone));
    pdat.year = tpa.year;
    pdat.day = tpa.day;
    pdat.month = tpa.month;
    pdat.hour = tpa.hour;
    pdat.minute = tpa.minute;
    pdat.second = tpa.second;
    auto retval = S_solpos(&pdat);
    if (retval != 0) {
        std::cout << "Solpos error" << std::endl;
        std::exit(EXIT_FAILURE);
    }
}

void setLocPoint(location &loc, utils::sectionconfigmaptxt &conf) {
    auto section = conf["location"];
    try {
        loc.lat = std::stod(section["latitude"]);
    } catch (std::invalid_argument const &ex) {
        std::cout << "Error: " << ex.what() << std::endl;
        std::cout << "Using default lat: 47" << std::endl;
        loc.lat = 47;
    }
    try {
        loc.lon = std::stod(section["longitude"]);
    } catch (std::invalid_argument const &ex) {
        std::cout << "Error: " << ex.what() << std::endl;
        std::cout << "Using default lon: 8" << std::endl;
        loc.lon = 8;
    }
    loc.altitude = 0;
    try {
        loc.tz = locate_zone(section["tz"]);
    } catch (std::invalid_argument const &ex) {
        std::cout << "Error: " << ex.what() << std::endl;
        std::cout << "Using default tz: Europe/Zurich" << std::endl;
        loc.tz = locate_zone("Europe/Zurich");
    }
}