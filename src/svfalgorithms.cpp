#include "svfalgorithms.hpp"
#include "svfcl.hpp"
#include "utils.hpp"

resultsVeg shadowingVeg(const Eigen::MatrixXf &dsm, const Eigen::MatrixXf &vegdem, const Eigen::MatrixXf &vegdem2,
                        const Eigen::MatrixXf &bush, float azi, int alti, float scale, int idx, int loopcounter) {
    double azimuth = azi * deg2rad;
    double altitude = alti * deg2rad;
    auto sizex = dsm.rows();
    auto sizey = dsm.cols();
    Eigen::MatrixXf f(dsm);
    double dx = 0, dy = 0, dz = 0, ds = 0;
    Eigen::MatrixXf temp = Eigen::MatrixXf::Constant(sizex, sizey, 0);
    Eigen::MatrixXf tempvegdem(temp);
    Eigen::MatrixXf tempvegdem2(temp);
    Eigen::MatrixXf templastfabovea(temp);
    Eigen::MatrixXf templastgabovea(temp);
    Eigen::MatrixXf bushplant = bush.unaryExpr([](float elem) { return elem > 1 ? 1.0f : 0.0f; });
    Eigen::MatrixXf sh(temp);
    Eigen::MatrixXf vbshvegsh(temp);
    Eigen::MatrixXf vegsh = Eigen::MatrixXf::Constant(sizex, sizey, 0).array() + bushplant.array();
    Eigen::MatrixXf vegsh2(temp);
    Eigen::MatrixXf gabovea(temp);
    Eigen::MatrixXf fabovea(temp);
    Eigen::MatrixXf lastfabovea(temp);
    Eigen::MatrixXf lastgabovea(temp);
    auto amaxvalue = dsm.maxCoeff();
    auto const sinazimuth = std::sin(azimuth);
    auto const cosazimuth = std::cos(azimuth);
    auto const tanazimuth = std::tan(azimuth);
    auto const signsinazimuth = signum(sinazimuth);
    auto const signcosazimuth = signum(cosazimuth);
    auto const dssin = std::abs((1. / sinazimuth));
    auto const dscos = std::abs((1. / cosazimuth));
    auto tanaltitudebyscale = std::tan(altitude) / scale;
    int index = 0;
    double dzprev = 0;
    while (amaxvalue >= dz && std::abs(dx) < sizex && std::abs(dy) < sizey) {
        if (pibyfour <= azimuth && azimuth < threetimespibyfour ||
            fivetimespibyfour <= azimuth && azimuth < seventimespibyfour) {
            dy = signsinazimuth * index;
            dx = -1. * signcosazimuth * std::abs(std::round(index / tanazimuth));
            ds = dssin;
        } else {
            dy = signsinazimuth * std::abs(std::round(index * tanazimuth));
            dx = -1. * signcosazimuth * index;
            ds = dscos;
        }
        dz = (ds * index) * tanaltitudebyscale;
        temp.array() = 0;
        tempvegdem.array() = 0;
        tempvegdem2.array() = 0;
        templastgabovea.array() = 0;
        templastfabovea.array() = 0;
        auto absdx = std::abs(dx);
        auto absdy = std::abs(dy);
        auto xc1 = static_cast<int>((dx + absdx) / 2.);
        auto xc2 = static_cast<int>(sizex + (dx - absdx) / 2.);
        auto yc1 = static_cast<int>((dy + absdy) / 2.);
        auto yc2 = static_cast<int>(sizey + (dy - absdy) / 2.);
        auto xp1 = static_cast<int>(-((dx - absdx) / 2.));
        auto xp2 = static_cast<int>(sizex - (dx + absdx) / 2.);
        auto yp1 = static_cast<int>(-((dy - absdy) / 2.));
        auto yp2 = static_cast<int>(sizey - (dy + absdy) / 2.);
        tempvegdem(Eigen::seq(xp1, xp2 - 1), Eigen::seq(yp1, yp2 - 1)) =
            vegdem(Eigen::seq(xc1, xc2 - 1), Eigen::seq(yc1, yc2 - 1)).array() - dz;
        tempvegdem2(Eigen::seq(xp1, xp2 - 1), Eigen::seq(yp1, yp2 - 1)) =
            vegdem2(Eigen::seq(xc1, xc2 - 1), Eigen::seq(yc1, yc2 - 1)).array() - dz;
        temp(Eigen::seq(xp1, xp2 - 1), Eigen::seq(yp1, yp2 - 1)) =
            dsm(Eigen::seq(xc1, xc2 - 1), Eigen::seq(yc1, yc2 - 1)).array() - dz;
        for (unsigned int row = 0; row != f.rows(); row++) {
            for (unsigned int column = 0; column != f.cols(); column++) {
                f(row, column) = f(row, column) >= temp(row, column) ? f(row, column) : temp(row, column);
            }
        }
        sh = (f.array() > dsm.array()).select(1, sh);
        sh = (f.array() <= dsm.array()).select(0, sh);
        for (unsigned int row = 0; row != fabovea.rows(); row++) {
            for (unsigned int col = 0; col != fabovea.cols(); col++) {
                fabovea(row, col) = tempvegdem(row, col) > dsm(row, col) ? 1.0 : 0.0;
                gabovea(row, col) = tempvegdem2(row, col) > dsm(row, col) ? 1.0 : 0.0;
            }
        }
        templastfabovea(Eigen::seq(xp1, xp2 - 1), Eigen::seq(yp1, yp2 - 1)) =
            vegdem(Eigen::seq(xc1, xc2 - 1), Eigen::seq(yc1, yc2 - 1)).array() - dzprev;
        templastgabovea(Eigen::seq(xp1, xp2 - 1), Eigen::seq(yp1, yp2 - 1)) =
            vegdem2(Eigen::seq(xc1, xc2 - 1), Eigen::seq(yc1, yc2 - 1)).array() - dzprev;
        for (unsigned int row = 0; row != templastgabovea.rows(); row++) {
            for (unsigned int col = 0; col != templastgabovea.cols(); col++) {
                lastfabovea(row, col) = templastfabovea(row, col) > dsm(row, col) ? 1.0 : 0.0;
                lastgabovea(row, col) = templastgabovea(row, col) > dsm(row, col) ? 1.0 : 0.0;
            }
        }
        dzprev = dz;
        vegsh2 = fabovea + gabovea + lastfabovea + lastgabovea;
        vegsh2 = (vegsh2.array() == 4).select(0, vegsh2);
        vegsh2 = (vegsh2.array() > 0).select(1, vegsh2);
        for (unsigned int row = 0; row != vegsh.rows(); row++) {
            for (unsigned int column = 0; column != vegsh.cols(); column++) {
                vegsh(row, column) =
                    vegsh(row, column) >= vegsh2(row, column) ? vegsh(row, column) : vegsh2(row, column);
            }
        }
        vegsh = (vegsh.array() * sh.array() > 0).select(0, vegsh);
        vbshvegsh = vegsh.array() + vbshvegsh.array();
        index++;
    }
    sh = 1 - sh.array();
    vbshvegsh = (vbshvegsh.array() > 0.).select(1, vbshvegsh);
    vbshvegsh = vbshvegsh - vegsh;
    vegsh = 1. - vegsh.array();
    vbshvegsh = 1. - vbshvegsh.array();
    resultsVeg myresults;
    myresults.shBuild = sh;
    myresults.shVeg = vegsh;
    myresults.shBushVeg = vbshvegsh;
    myresults.idx = idx;
    myresults.loopcounter = loopcounter;
    myresults.azimuth = azi;
    std::cout << idx << " done." << std::endl;
    return myresults;
}

resultNonVeg shadowingGlobalRadiation(const Eigen::MatrixXf &dsm, float azi, int alti, float scale, int idx,
                                      int loopcounter) {
    double azimuth = azi * deg2rad;
    double altitude = alti * deg2rad;
    auto sizex = dsm.rows();
    auto sizey = dsm.cols();
    Eigen::MatrixXf f(dsm);
    double dx = 0, dy = 0, dz = 0, ds = 0;
    Eigen::MatrixXf temp = Eigen::MatrixXf::Constant(sizex, sizey, 0);
    int index = 1;
    auto amaxvalue = dsm.maxCoeff();
    auto const sinazimuth = std::sin(azimuth);
    auto const cosazimuth = std::cos(azimuth);
    auto const tanazimuth = std::tan(azimuth);
    auto const signsinazimuth = signum(sinazimuth);
    auto const signcosazimuth = signum(cosazimuth);
    auto const dssin = std::abs((1. / sinazimuth));
    auto const dscos = std::abs((1. / cosazimuth));
    auto tanaltitudebyscale = std::tan(altitude) / scale;
    while (amaxvalue >= dz && std::abs(dx) < sizex && std::abs(dy) < sizey) {
        if (pibyfour <= azimuth && azimuth < threetimespibyfour ||
            fivetimespibyfour <= azimuth && azimuth < seventimespibyfour) {
            dy = signsinazimuth * index;
            dx = -1. * signcosazimuth * std::abs(std::round(index / tanazimuth));
            ds = dssin;
        } else {
            dy = signsinazimuth * std::abs(std::round(index * tanazimuth));
            dx = -1. * signcosazimuth * index;
            ds = dscos;
        }
        dz = ds * index * tanaltitudebyscale;
        temp.array() = 0;
        auto absdx = std::abs(dx);
        auto absdy = std::abs(dy);
        auto xc1 = static_cast<int>((dx + absdx) / 2. + 1);
        auto xc2 = static_cast<int>(sizex + (dx - absdx) / 2.);
        auto yc1 = static_cast<int>((dy + absdy) / 2. + 1.);
        auto yc2 = static_cast<int>(sizey + (dy - absdy) / 2.);
        auto xp1 = static_cast<int>(-((dx - absdx) / 2.) + 1.);
        auto xp2 = static_cast<int>(sizex - (dx + absdx) / 2.);
        auto yp1 = static_cast<int>(-((dy - absdy) / 2.) + 1.);
        auto yp2 = static_cast<int>(sizey - (dy + absdy) / 2.);
        temp(Eigen::seq(xp1 - 1, xp2 - 1), Eigen::seq(yp1 - 1, yp2 - 1)) =
            (dsm(Eigen::seq(xc1 - 1, xc2 - 1), Eigen::seq(yc1 - 1, yc2 - 1)).array() - dz);
        for (unsigned int row = 0; row != f.rows(); row++) {
            for (unsigned int column = 0; column != f.cols(); column++) {
                f(row, column) = f(row, column) >= temp(row, column) ? f(row, column) : temp(row, column);
            }
        }
        index++;
    }
    f = (f - dsm).eval();
    f = f.unaryExpr([](float elem) { return static_cast<float>(!elem); });
    std::cout << idx << " done." << std::endl;
    return resultNonVeg{.shBuild = f, .idx = idx, .loopcounter = loopcounter, .azimuth = azi};
}

void calculateSvfDSM(patches &mypatch, svfnonveg &svf, Eigen::VectorXf &aziintervalaniso,
                     std::deque<std::future<resultNonVeg>> &myresults) {
    auto ii = 0;
    while (!myresults.empty()) {
        auto calcres = myresults.back().get();
        myresults.pop_back();
        auto ann = arange(mypatch.annulino(calcres.loopcounter) + 1, mypatch.annulino(calcres.loopcounter + 1) + 1);
        for (auto k : ann) {
            auto weight = annulus_weight(k, mypatch.patches_in_band(calcres.loopcounter)) * calcres.shBuild;
            svf.svf = (svf.svf + weight).eval();
            auto weight2 = annulus_weight(k, aziintervalaniso(calcres.loopcounter)) * calcres.shBuild;
            if (calcres.azimuth >= 0 && calcres.azimuth < 180) {

                svf.svfE = (svf.svfE + weight2).eval();
            }
            if (calcres.azimuth >= 90 && calcres.azimuth < 270) {

                svf.svfS = (svf.svfS + weight2).eval();
            }
            if (calcres.azimuth >= 180 && calcres.azimuth < 360) {

                svf.svfW = (svf.svfW + weight2).eval();
            }
            if (calcres.azimuth >= 270 || calcres.azimuth < 90) {

                svf.svfN = (svf.svfN + weight2).eval();
            }
        }
        std::cout << "Task: " << ii << std::endl;
        ii++;
    }
}
void calculateSvfVegDSM(patches &mypatch, svfvegbuild &svf, Eigen::VectorXf &aziintervalaniso,
                        std::deque<std::future<resultsVeg>> &myresults) {
    auto ii = 0;
    while (!myresults.empty()) {
        auto calcres = myresults.back().get();
        myresults.pop_back();
        auto ann = arange(mypatch.annulino(calcres.loopcounter) + 1, mypatch.annulino(calcres.loopcounter + 1) + 1);
        for (auto k : ann) {
            // auto weight = annulus_weight(k, aziinterval(calcres.loopcounter));
            auto weight = annulus_weight(k, mypatch.patches_in_band(calcres.loopcounter));
            svf.svf = (svf.svf + weight * calcres.shBuild).eval();
            svf.svfveg = (svf.svfveg + weight * calcres.shVeg).eval();
            svf.svfaveg = (svf.svfaveg + weight * calcres.shBushVeg).eval();
            auto weight2 = annulus_weight(k, aziintervalaniso(calcres.loopcounter));
            if (calcres.azimuth >= 0 && calcres.azimuth < 180) {

                svf.svfEveg = (svf.svfEveg.array() + weight2 * calcres.shVeg.array()).eval();
                svf.svfEaveg = (svf.svfEaveg.array() + weight2 * calcres.shBushVeg.array()).eval();
                svf.svfE = (svf.svfE.array() + weight2 * calcres.shBuild.array()).eval();
            }
            if (calcres.azimuth >= 90 && calcres.azimuth < 270) {

                svf.svfSveg = (svf.svfSveg.array() + weight2 * calcres.shVeg.array()).eval();
                svf.svfSaveg = (svf.svfSaveg.array() + weight2 * calcres.shBushVeg.array()).eval();
                svf.svfS = (svf.svfS.array() + weight2 * calcres.shBuild.array()).eval();
            }
            if (calcres.azimuth >= 180 && calcres.azimuth < 360) {
                svf.svfWveg = (svf.svfWveg.array() + weight2 * calcres.shVeg.array()).eval();
                svf.svfWaveg = (svf.svfWaveg.array() + weight2 * calcres.shBushVeg.array()).eval();
                svf.svfW = (svf.svfW.array() + weight2 * calcres.shBuild.array()).eval();
            }
            if (calcres.azimuth >= 270 || calcres.azimuth < 90) {
                svf.svfNveg = (svf.svfNveg.array() + weight2 * calcres.shVeg.array()).eval();
                svf.svfNaveg = (svf.svfNaveg.array() + weight2 * calcres.shBushVeg.array()).eval();
                svf.svfN = (svf.svfN.array() + weight2 * calcres.shBuild.array()).eval();
            }
        }
        std::cout << "Task: " << ii << std::endl;
        ii++;
    }
}
