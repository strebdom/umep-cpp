#ifndef __GIT_UMEP_CPP_INCLUDE_SOLWEIG_INTERFACE_HPP_
#define __GIT_UMEP_CPP_INCLUDE_SOLWEIG_INTERFACE_HPP_



#include "unistd.h"
#include <string>
#include <iostream>
#include "common_utils.hpp"


const std::map<std::string,std::vector<std::string>> requiredParameters{{"location", {"longitude", "latitude", "tz"}}};


void checkConfig(utils::sectionconfigmaptxt &conf);

std::string getOptions(int &argc, char **argv);
void showHelp();


#endif // __GIT_UMEP_CPP_INCLUDE_SOLWEIG_INTERFACE_HPP_