#ifndef __GIT_UMEP_CPP_INCLUDE_SOLWEIG_HPP_
#define __GIT_UMEP_CPP_INCLUDE_SOLWEIG_HPP_
#include <chrono>
#include "common_utils.hpp"
#include "solpos.h"
#include <iostream>
#include <sstream>
#include <string>
#include <tuple>
#include "date/tz.h"
#include "date/date.h"


struct location {

    double lat, lon, altitude;
    const date::time_zone *tz;
};

struct tpSol {
    int year, month, day, hour, minute, second;
};


void getSolarParameters(posdata &pdat, tpSol &tp, location &lc);

void setLocPoint(location &loc, utils::sectionconfigmaptxt &conf);

template<class T, class V> 
void printDate(date::zoned_time<T,V> &timein){
    auto dp = date::floor<date::days>(timein.get_local_time());
    auto ymd = date::year_month_day{dp};
    auto time = date::make_time(std::chrono::duration_cast<std::chrono::milliseconds>(timein.get_local_time()-dp));    
    std::cout << "year        = " << ymd.year() << '\n';
    std::cout << "month       = " << ymd.month() << '\n';
    std::cout << "day         = " << ymd.day() << '\n';
    std::cout << "hour        = " << time.hours().count() << "h\n";
    std::cout << "minute      = " << time.minutes().count() << "min\n";
    std::cout << "second      = " << time.seconds().count() << "s\n";
    std::cout << "millisecond = " << time.subseconds().count() << "ms\n";
}



template<class T, class V> 
auto getDateTimeComponents(date::zoned_time<T,V> &timein){
    auto dp = date::floor<date::days>(timein.get_local_time());
    auto ymd = date::year_month_day{dp};
    auto time = date::make_time(std::chrono::duration_cast<std::chrono::milliseconds>(timein.get_local_time()-dp));
    std::stringstream year_ss,month_ss, day_ss;
    month_ss << date::format("%m", ymd);
    day_ss << ymd.day();
    year_ss << ymd.year();
    return std::make_tuple(std::stoi(year_ss.str()), std::stoi(month_ss.str()), std::stoi(day_ss.str()), time.hours().count(), time.minutes().count(),
                                                        time.seconds().count(), time.subseconds().count());
}


template <class Rep, std::intmax_t num, std::intmax_t denom>
auto chronoBurst(std::chrono::duration<Rep, std::ratio<num, denom>> d)
{
    const auto hrs = duration_cast<std::chrono::hours>(d);
    const auto mins = duration_cast<std::chrono::minutes>(d - hrs);
    const auto secs = duration_cast<std::chrono::seconds>(d - hrs - mins);
    const auto ms = duration_cast<std::chrono::milliseconds>(d - hrs - mins - secs);

    return std::make_tuple(hrs, mins, secs, ms);
}

#endif