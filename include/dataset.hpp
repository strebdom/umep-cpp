#ifndef __GIT_UMEP_CPP_INCLUDE_DATASET_HPP_
#define __GIT_UMEP_CPP_INCLUDE_DATASET_HPP_
#include "cpl_conv.h"
#include "gdal_priv.h"
#include <Eigen/Dense>
#include <iostream>
#include <string>

class Dataset {
  private:
    GDALDataset *dset;
    GDALRasterBand *band;
    std::string driver;

  public:
    Dataset();
    Dataset(std::string &path);
    void init(std::string &path);
    Eigen::Map<Eigen::MatrixXf> getEigenMatrix();
    ~Dataset();
    friend std::ostream &operator<<(std::ostream &os, const Dataset &ds);
    double geoTransform[6];
    const std::string path;
    std::string projref;
    int nBlockXSize, nBlockYSize, XSize, YSize, RasterCount;
};

#endif // __GIT_UMEP_CPP_INCLUDE_DATASET_HPP_