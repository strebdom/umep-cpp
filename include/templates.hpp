#ifndef __GIT_SKYVIEWFACTORS_TEMPLATES_HPP_
#define __GIT_SKYVIEWFACTORS_TEMPLATES_HPP_

#include <type_traits>
#include <vector>

template <typename T> std::vector<T> arange(T start, T stop, T step = 1) {
    std::vector<T> values;
    for (T value = start; value < stop; value += step)
        values.push_back(value);
    return values;
}

template <typename T> inline constexpr int signum(T x, std::false_type is_signed) {
    return T(0) < x;
}

template <typename T> inline constexpr int signum(T x, std::true_type is_signed) {
    return (T(0) < x) - (x < T(0));
}

template <typename T> inline constexpr int signum(T x) {
    return signum(x, std::is_signed<T>());
}
#endif // __GIT_SKYVIEWFACTORS_TEMPLATES_HPP_
