#ifndef __GIT_UMEP_CPP_INCLUDE_FINDWALLS_HPP_
#define __GIT_UMEP_CPP_INCLUDE_FINDWALLS_HPP_

#ifdef _WIN32
#include "getopt.hpp"
#endif

#ifdef __APPLE__
#include "unistd.h"
#endif

#ifdef __linux__
#include "unistd.h"
#endif
#include "utils.hpp"
#include <string>
#include <Eigen/Dense>
#include <tuple>
#include "dataset.hpp"

struct wallconfig {
    std::string in;
    std::string out;
    bool calculateAspect;
    double walllimit;
};

Eigen::MatrixXf getWalls(Eigen::Map<Eigen::MatrixXf>  &dsm, double walllimit);
Eigen::MatrixXf getAspect(Eigen::Map<Eigen::MatrixXf> &dsm,Eigen::MatrixXf &walls, double scale);
std::tuple<Eigen::MatrixXf, Eigen::MatrixXf> getDers(Eigen::Map<Eigen::MatrixXf> &dsm, double scale);
std::tuple<Eigen::MatrixXf, Eigen::MatrixXf> getGradient(Eigen::Map<Eigen::MatrixXf> &dsm, double dx, double dy);
std::tuple<Eigen::MatrixXf, Eigen::MatrixXf> cart2pol(Eigen::MatrixXf &x, Eigen::MatrixXf &y);
Eigen::MatrixXf getRotatedMatrix(Eigen::MatrixXf &matrix, int angle);
wallconfig getOptions(int &argc, char **argv);

#endif // __GIT_UMEP_CPP_INCLUDE_FINDWALLS_HPP_