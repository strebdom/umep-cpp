#ifndef __UMEP_SKYVIEWFACTORS_HPP_
#define __UMEP_SKYVIEWFACTORS_HPP_
#include "BS_thread_pool.hpp"
#include "cmath"
#include "cpl_conv.h"
#include "dataset.hpp"
#include "gdal_priv.h"
#include "svfalgorithms.hpp"
#include "templates.hpp"
#include "utils.hpp"
#include <Eigen/Dense>
#include <deque>
#include <future>
#include <vector>
#include "svfcl.hpp"

struct svfconfig {
    std::string in_dsm;
    std::string in_veg;
    std::string out;
    bool isVeg;
    float trunkratio;
    float transmissivity;
    int patchoption;
};

void svfdsm(const svfconfig &myconf, Dataset &DSMBuildDataset);

void svfdsmveg(const svfconfig &myconf, Dataset &DSMBuildDataset, Dataset &VegDataset);

void submitJobsDSM(double *geoTransform, Eigen::Map<Eigen::MatrixXf> &dsm, patches &mypatch, int &index,
                   Eigen::Map<Eigen::VectorXf> &iazimuth, std::deque<std::future<resultNonVeg>> &myresults,
                   BS::thread_pool &mythreads);

void submitJobsDSMVeg(double *geoTransform, Eigen::Map<Eigen::MatrixXf> &dsm, Eigen::MatrixXf &veg,
                      Eigen::MatrixXf &veg2, Eigen::MatrixXf &bush, patches &mypatch, int &index,
                      Eigen::Map<Eigen::VectorXf> &iazimuth, std::deque<std::future<resultsVeg>> &myresults,
                      BS::thread_pool &mythreads);

svfconfig getOptions(int &argc, char **argv);

#endif // __UMEP_SKYVIEWFACTORS_HPP_