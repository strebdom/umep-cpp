#ifndef __GIT_UMEP_CPP_INCLUDE_WRFNCDATASET_HPP_
#define __GIT_UMEP_CPP_INCLUDE_WRFNCDATASET_HPP_

#include "Eigen/Dense"
#include "common_utils.hpp"
#include "cpl_string.h"
#include "gdal.h"
#include "gdal_priv.h"
#include "gdalwarper.h"
#include "ogr_spatialref.h"
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <sys/stat.h>
#include <unistd.h>
#include "projection.hpp"

using Datamatrix = Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>;

class WRFNCDataset {
  public:
    WRFNCDataset(const std::string &filename, const int debug);
    ~WRFNCDataset();
    std::vector<std::string> getVars();
      const std::vector<std::string> &getVarsref();
    std::map<std::string,std::unique_ptr<Datamatrix>> dims;
    std::vector<std::array<double, 6>> getGT();
    std::vector<OGRSpatialReference> getOSR();
    int nx, ny;

  private:
    std::string filename;
    const int debug;
    GDALDataset *mainds;
    std::unique_ptr<Projection> proj;
    std::map<std::string, GDALDataset*> vsds;
    std::map<std::string, GDALRasterBand*> rsmap;
    std::vector<std::string> ncvars;
    const std::vector<std::string> MANDATORY_VARIABLES{"XLAT", "XLONG", "XLAT_U", "XLAT_V", "XLONG_U", "XLONG_V"};

    inline bool file_exists(const std::string &name);
    bool checkVariable(const std::string &var);
    GDALDataset *openDataset(const std::string &filename);
    std::vector<std::string> getNcvars();
    const std::vector<std::string> &getNcvarsref();
    void checkDatasetConformity();
    void checkMandatoryVariables();
    inline void exitWRFDataset(const std::string &message);
    void initializeMandatoryVariables();


};

#endif // __GIT_UMEP_CPP_INCLUDE_WRFNCDATASET_HPP_