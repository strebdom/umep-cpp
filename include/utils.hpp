#ifndef __GIT_SKYVIEWFACTORS_UTILS_HPP_
#define __GIT_SKYVIEWFACTORS_UTILS_HPP_

#include "cpl_conv.h"
#include "dataset.hpp"
#include "gdal_priv.h"
#include "templates.hpp"
#include <Eigen/Dense>
#include <iostream>
#include <vector>
#include "svfcl.hpp"

constexpr auto deg2rad = M_PI / 180.0;
constexpr auto pibyfour = M_PI / 4.;
constexpr auto threetimespibyfour = 3. * pibyfour;
constexpr auto fivetimespibyfour = 5. * pibyfour;
constexpr auto seventimespibyfour = 7. * pibyfour;

using aziVec = Eigen::VectorXf;
using aziVecMap = Eigen::Map<Eigen::VectorXf>;

struct patches {
    Eigen::VectorXi skyvaultalt;
    Eigen::VectorXf skyvaultazi;
    Eigen::VectorXf annulino;
    Eigen::VectorXi skyvaultaltint;
    Eigen::VectorXi patches_in_band;
    Eigen::VectorXf skyvaultaziint;
    Eigen::VectorXi azistart;
};

using angleresult=std::tuple<aziVec,aziVec>;

struct resultNonVeg {
    Eigen::MatrixXf shBuild;
    int idx;
    int loopcounter;
    double azimuth;
};

struct resultsVeg : public resultNonVeg {
    Eigen::MatrixXf shVeg;
    Eigen::MatrixXf shBushVeg{};
};

patches getPatches(unsigned int option);
void iAzi(patches &mypatch, std::vector<float> &temp);
void normToOne(svfnonveg &svf);
void normToOne(svfvegbuild &svf);

double annulus_weight(float altitude, float aziinterval);
void outputPatches(patches &mypatch);
void saveNewFile(Dataset &ds, Eigen::MatrixXf &svf, const char *destfile);

angleresult svf_angles_100121();

#endif // __GIT_SKYVIEWFACTORS_UTILS_HPP_