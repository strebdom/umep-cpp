//
// Created by Dominik Strebel on 10.11.22.
//

#ifndef UMEP_CPP_SVFCL_HPP
#define UMEP_CPP_SVFCL_HPP
#include "Eigen/Dense"


class svfnonveg {
  public:
    int rows, cols;
    Eigen::MatrixXf svf;
    Eigen::MatrixXf svfE;
    Eigen::MatrixXf svfS;
    svfnonveg(int rows, int cols);
    Eigen::MatrixXf svfW;
    Eigen::MatrixXf svfN;

};


class svfvegbuild: public svfnonveg {

  public:
    int rows, cols;
    svfvegbuild(int rows, int cols);
    Eigen::MatrixXf svfveg;
    Eigen::MatrixXf svfEveg;
    Eigen::MatrixXf svfSveg;
    Eigen::MatrixXf svfWveg;
    Eigen::MatrixXf svfNveg;
    Eigen::MatrixXf svfaveg;
    Eigen::MatrixXf svfEaveg;
    Eigen::MatrixXf svfSaveg;
    Eigen::MatrixXf svfWaveg;
    Eigen::MatrixXf svfNaveg;
};



#endif // UMEP_CPP_SVFCL_HPP
