#ifndef __GIT_UMEP_CPP_INCLUDE_WRF_TO_GEOTIFF_HPP_
#define __GIT_UMEP_CPP_INCLUDE_WRF_TO_GEOTIFF_HPP_
#include "common_utils.hpp"
#include "cpl_string.h"
#include "gdal.h"
#include "gdal_priv.h"
#include "gdalwarper.h"
#include "ogr_spatialref.h"
#include <Eigen/Dense>
#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <map>
#include <memory>

using Eigen::placeholders::last;
using flimit = std::numeric_limits<float>;



#endif // __GIT_UMEP_CPP_INCLUDE_WRF_TO_GEOTIFF_HPP_