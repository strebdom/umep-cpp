#ifndef __GIT_UMEP_CPP_INCLUDE_SVFALGORITHMS_HPP_
#define __GIT_UMEP_CPP_INCLUDE_SVFALGORITHMS_HPP_
#include "Eigen/Dense"
#include "svfcl.hpp"
#include "utils.hpp"
#include <deque>
#include <future>
#include <string>

resultNonVeg shadowingGlobalRadiation(const Eigen::MatrixXf &dsm, float azi, int alti, float scale, int idx,
                                      int loopcounter);
resultsVeg shadowingVeg(const Eigen::MatrixXf &dsm, const Eigen::MatrixXf &vegdem, const Eigen::MatrixXf &vegdem2,
                        const Eigen::MatrixXf &bush, float azi, int alti, float scale, int idx, int loopcounter);
void calculateSvfDSM(patches &mypatch, svfnonveg &svf, Eigen::VectorXf &aziintervalaniso,
                     std::deque<std::future<resultNonVeg>> &myresults);

void calculateSvfVegDSM(patches &mypatch, svfvegbuild &svf, Eigen::VectorXf &aziintervalaniso,
                     std::deque<std::future<resultsVeg>> &myresults);


#endif // __GIT_UMEP_CPP_INCLUDE_SVFALGORITHMS_HPP_

