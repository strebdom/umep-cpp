#ifndef __GIT_UMEP_CPP_LIB_UMCPC_COMMON_INCLUDE_PROJECTION_HPP_
#define __GIT_UMEP_CPP_LIB_UMCPC_COMMON_INCLUDE_PROJECTION_HPP_
#include "common_utils.hpp"
#include "cpl_string.h"
#include "gdal.h"
#include "gdal_priv.h"
#include "gdalwarper.h"
#include "ogr_spatialref.h"
#include <Eigen/Dense>
#include <cwchar>
#include <iostream>
#include <memory>
#include <sstream>

enum PROJTYPE {
    LAMBERT_CONFORMAL = 1,
    POLAR_STEREOGRAPHIC = 2,
    MERCATOR = 3,
    LAT_LON = 6
};

class Projection {

    using Geotransform = std::array<double, 6>;
    using Datamatrix = Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>>;
    const std::string netCDFString = R"(NETCDF:")";

  public:
    Projection(std::map<std::string, GDALDataset*> &dimds, const int debug);
    Projection(Projection &&) = default;
    Projection &operator=(Projection &&) = default;
    ~Projection()=default;
  friend class WRFNCDataset;

  private:
    struct Coordinate {
        double x;
        double y;
    };
    int debug;
    double dx,dy;
    /*struct ImageParams {
        Coordinate llu, lru, llv, ulv;
        int XSize, YSize, nVals; 
    };*/
    struct ImageParams {
        Coordinate ul, ur, ll, lr;
        int XSize, YSize, nVals; 
    };
    std::map<std::string, GDALDataset*> dimds;
    std::string filename;
    Geotransform orig_gt, transformed_gt;
    std::string getProjParams();
    ImageParams imgparams_orig, imgparams_out;
    std::string getLambertProjParams(std::map<std::string, std::string> &params);
    void initDataMatrices();
    void initSize();
    void prepareBuffer();
    void getOrigImageCorners();
    void initTransformer();
    void transformImageCorners(ImageParams &parmin, ImageParams &parmout);
    Geotransform getLatLonGT(ImageParams &parm);
    Geotransform getProjectedGT(ImageParams &parm);

    std::unique_ptr<float[]> XLATBuffer, XLONGBuffer, XLONGUBuffer, XLONGVBuffer, XLATUBuffer, XLATVBuffer;
    OGRSpatialReference osr_latlon, osr_out;
    OGRCoordinateTransformation *transformer;
    std::unique_ptr<Datamatrix> XLATData, XLONGData, XLONGUData, XLONGVData, XLATUData, XLATVData;
};

#endif // __GIT_UMEP_CPP_LIB_UMCPC_COMMON_INCLUDE_PROJECTION_HPP_