//
// Created by Dominik Strebel on 09.11.22.
//

#include <iostream>
#include "../include/templates.hpp"

int main() {
    auto noa = 19.0;
    auto step = 89.0 / noa;
    auto iangle = arange(step / 2, 89., step);
    auto annulino = arange(0.0, 89.0, step);
    iangle.push_back(90.0);
    annulino.push_back(90.0);
    for (auto elem: iangle){
        std::cout << elem << " ";
    }
    std::cout << std::endl;
    for(auto elem: annulino){
        std::cout << elem << " ";
    }
    std::cout << std::endl;
}