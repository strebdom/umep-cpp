#include "CImg.h"
#ifdef Success
  #undef Success
#endif
#include <Eigen/Dense>
#include <Eigen/src/Core/Matrix.h>
#include <Eigen/src/Core/util/Constants.h>
#include <iostream>

// Performs an arbitrary affine transformation on the image.

// This example relies on the matrices and functions available in GIL to define the operation,
// in include/boost/gil/extension/numeric/affine.hpp
// and calls resample_pixels(), avaiable in the numeric extension, to apply it

using namespace cimg_library;

int main(int argc, char *argv[]) {
    double scale = 2;
    auto filtersize = std::floor((scale + 0.0000000001) * 9);
    if (filtersize != 9) {
        if (static_cast<int>(filtersize) % 2 == 0) {
            filtersize++;
        }
    }
      auto filthalveceil = static_cast<int>(std::ceil(filtersize / 2.));
    auto filthalvefloor = static_cast<int>(std::floor(filtersize / 2.));
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> filtmatrix =
        Eigen::MatrixXf::Constant(filtersize, filtersize, 0);
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> buildfilt =
        Eigen::MatrixXf::Constant(filtersize, filtersize, 0);
    filtmatrix(Eigen::all, filthalveceil - 1).array() = 1.0f;
    auto shape = filtmatrix.rows();
    buildfilt(filthalveceil - 1, Eigen::seq(0, filthalvefloor-1)).array() = 1.0f;
    buildfilt(filthalveceil - 1, Eigen::seq(filthalveceil, filtersize - 1)).array() = 2.0f;
    int rotation = std::atoi(argv[1]);
    int bc = std::atoi(argv[2]);

    //CImg<float> img(buildfilt.data(), buildfilt.cols(), buildfilt.rows(), 1, 1, true);
    CImg<float> img(buildfilt.cols(), buildfilt.cols(),1,1);
    for (auto i=0; i!=buildfilt.rows(); i++){
        for(auto j=0; j!=buildfilt.cols(); j++){
            img(j,i) = buildfilt(i,j);
        }
    }
    img.rotate(rotation, static_cast<int>((img.width() - 1) / 2), static_cast<int>((img.height() - 1) / 2), 0, bc);
    Eigen::MatrixXf newm(img.width(), img.height());

    for (auto i=0; i!=img.width(); i++){
        for(auto j = 0; j!=img.height(); j++) {
            newm(j,i) = img(i,j);
        } 
    }
    std::cout << "Width: " << img.width() << " Height: " << img.height() << std::endl;
    img.display();
    std::cout << newm << std::endl;
    // test resample_pixels
    // Transform the image by an arbitrary affine transformation using nearest-neighbor resampling
    /*gil::rgb8_image_t transf(gil::rgb8_image_t::point_t(gil::view(img).dimensions() * 2));
    gil::fill_pixels(gil::view(transf), gil::rgb8_pixel_t(255, 0, 0)); // the background is red

    gil::matrix3x2<double> mat =
        gil::matrix3x2<double>::get_translate(-gil::point<double>(200,250)) *
        gil::matrix3x2<double>::get_rotate(-15*3.14/180.0);
    gil::resample_pixels(const_view(img), gil::view(transf), mat, gil::nearest_neighbor_sampler());
    gil::write_view("out-affine.jpg", gil::view(transf), gil::jpeg_tag());*/

    return 0;
}