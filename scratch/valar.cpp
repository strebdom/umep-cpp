#include <iostream>
#include <valarray>


int main(){
    std::valarray<double> as{12,12,3,4,445,6,56,6,3};
    std::valarray<double> iss{12,34,54,56,34,23,3,56,11};
    auto mm =iss[std::slice(0,3,4)];
    std::cout << mm[1] << std::endl;
}