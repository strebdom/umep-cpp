#include "projection.hpp"

namespace projection{
Geotransform getGT(projection::ImageParams &params) {
    Geotransform gt;
    gt[0] = params.ul.x;
    gt[1] = (params.ur.x - gt[0]) / params.XSize;
    // gt[2] = (params.lr.x - gt[0] - gt[1] * params.XSize) / params.YSize;
    gt[2] = 0;
    gt[3] = params.ul.y;
    gt[5] = (params.ll.y - gt[3]) / params.YSize;
    gt[4] = 0;
    // gt[4] = (params.lr.y - gt[3] - gt[5] * params.YSize) / params.XSize;
    return gt;
}

std::string getLambertProjParams(std::map<std::string, std::string> &param) {
    std::stringstream projstringstream, projstringstream_wgs84;
    projstringstream << "+proj=lcc +units=m +a=6370000.0 +b=6370000.0 +lat_1=" << param["TRUELAT1"]
                     << " +lat_2=" << param["TRUELAT2"] << " +lat_0=" << param["MOAD_CEN_LAT"]
                     << " +lon_0=" << param["STAND_LON"];
    projstringstream_wgs84 << "+proj=lcc +units=m +lat_1=" << param["TRUELAT1"]
                     << " +lat_2=" << param["TRUELAT2"] << " +lat_0=" << param["MOAD_CEN_LAT"]
                     << " +lon_0=" << param["STAND_LON"] << " datum=WGS84";
    return projstringstream.str();
}

std::string getProjParams(std::unique_ptr<GDALDataset> &ds) {
    auto projnumstr = ds->GetMetadataItem("NC_GLOBAL#MAP_PROJ");
    int projnum;
    std::map<std::string, std::string> paramlist;
    std::string projstring;
    try {
        projnum = std::stoi(projnumstr);
    } catch (const std::invalid_argument &e) {
        std::cout << e.what() << std::endl;
        exit(EXIT_FAILURE);
    } catch (const std::out_of_range &e) {
        std::cout << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    switch (projnum) {
    case PROJTYPE::LAMBERT_CONFORMAL:
        paramlist["TRUELAT1"] = std::string(ds->GetMetadataItem("NC_GLOBAL#TRUELAT1"));
        paramlist["TRUELAT2"] = std::string(ds->GetMetadataItem("NC_GLOBAL#TRUELAT2"));
        paramlist["STAND_LON"] = std::string(ds->GetMetadataItem("NC_GLOBAL#STAND_LON"));
        paramlist["MOAD_CEN_LAT"] = std::string(ds->GetMetadataItem("NC_GLOBAL#MOAD_CEN_LAT"));
        projstring = getLambertProjParams(paramlist);
        break;
    case PROJTYPE::LAT_LON:
    case PROJTYPE::MERCATOR:
    case PROJTYPE::POLAR_STEREOGRAPHIC:
    default:
        std::cout << "Other projections than Lambert Conformal not yet implemented!" << std::endl;
        exit(EXIT_FAILURE);
    }
    return projstring;
}

};
