#ifndef __GIT_UMEP_CPP_LIB_UMCPC_COMMON_INCLUDE_PROJECTION_HPP_
#define __GIT_UMEP_CPP_LIB_UMCPC_COMMON_INCLUDE_PROJECTION_HPP_
#include "common_utils.hpp"
#include "cpl_string.h"
#include "gdal.h"
#include "gdal_priv.h"
#include "gdalwarper.h"
#include "ogr_spatialref.h"
#include <iostream>
#include <sstream>

namespace projection {
enum PROJTYPE {
    LAMBERT_CONFORMAL = 1,
    POLAR_STEREOGRAPHIC = 2,
    MERCATOR = 3,
    LAT_LON = 6
};
using Geotransform = std::array<double, 6>;

struct Coordinate {
    double x;
    double y;
};

struct ImageParams {
    Coordinate ul, lr, ur, ll;
    int XSize, YSize;
    std::vector<double> translon, translat;
};

Geotransform getGT(projection::ImageParams &params);
std::string getLambertProjParams(std::map<std::string, std::string> &params);
std::string getProjParams(std::unique_ptr<GDALDataset> &ds);
} // namespace projection

#endif // __GIT_UMEP_CPP_LIB_UMCPC_COMMON_INCLUDE_PROJECTION_HPP_