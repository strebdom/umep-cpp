#ifndef __GIT_COMMONUTILS_INCLUDE_UTILS_H_
#define __GIT_COMMONUTILS_INCLUDE_UTILS_H_

#include "gdal.h"
#include "gdal_priv.h"
#include <cstddef>
#include <cstring>
#include <iostream>
#include <map>
#include <regex>
#include <string>
#include <variant>

namespace utils {
using configmap = std::map<std::string, std::string>;
using sectionconfigmap = std::map<std::string, std::map<std::string, std::variant<std::string, int, double>>>;
using sectionconfigmaptxt = std::map<std::string, std::map<std::string, std::string>>;
using sectionmap = std::map<std::string, std::string>;
configmap parseConfig(std::string filename);
sectionconfigmaptxt parseSectionConfig(std::string filename);
bool detectNumber(const std::string &str);
static inline void ltrim(std::string &s);
static inline void rtrim(std::string &s);
static inline void trim(std::string &s);
static inline std::string ltrim_copy(std::string s);
static inline std::string rtrim_copy(std::string s);
static inline std::string trim_copy(std::string s);
std::string getLambertProjParams(std::map<std::string, std::string> &params);
std::string getProjParams(std::unique_ptr<GDALDataset> &ds);
template <typename T> T getValueFromConfMap(const sectionconfigmap &conf, std::string section, std::string key) {
    return std::get<T>(conf.at(section).at(key));
}

/*

template <typename T> void flip2DBuffer(T buffer, size_t rows, size_t cols) {
    auto half = rows / 2;
    typedef typename std::remove_reference<decltype(*std::declval<T>())>::type t_row;
    auto temprow = new t_row[cols];
    for (int i = 0; i < half; i++) {
        std::memcpy(temprow, &buffer[i * cols], cols);
        std::memcpy(&buffer[i*cols], &buffer[(rows-i) * cols], cols);
        std::memcpy(&buffer[(rows - i) * cols], temprow, cols);
        std::cout << temprow[0] << std::endl;
    }
    delete[] temprow;
}*/

template <typename T>
void flip_horizontal(const int nrows, const int ncols, T *data) // flips left-right
{
    for (int rr = 0; rr < nrows; rr++) {
        for (int cc = 0; cc < ncols / 2; cc++) {
            int ccInv = ncols - 1 - cc;
            std::swap<T>(data[rr * ncols + cc], data[rr * ncols + ccInv]);
        }
    }
}

template <typename T>
void flip_vertical(const int nrows, const int ncols, T *data) // flips: bottom-up
{
    for (int cc = 0; cc < ncols; cc++) {
        for (int rr = 0; rr < nrows / 2; rr++) {
            int rrInv = nrows - 1 - rr;
            std::swap<T>(data[rr * ncols + cc], data[rrInv * ncols + cc]);
        }
    }
}

} // namespace utils

#endif // __GIT_METEOBIKE_INCLUDE_UTILS_H_
